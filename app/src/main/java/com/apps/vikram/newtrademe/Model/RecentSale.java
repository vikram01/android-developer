package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class RecentSale {

private String ListingId;
private String Address;
private Integer SalePrice;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The ListingId
*/
public String getListingId() {
return ListingId;
}

/**
* 
* @param ListingId
* The ListingId
*/
public void setListingId(String ListingId) {
this.ListingId = ListingId;
}

/**
* 
* @return
* The Address
*/
public String getAddress() {
return Address;
}

/**
* 
* @param Address
* The Address
*/
public void setAddress(String Address) {
this.Address = Address;
}

/**
* 
* @return
* The SalePrice
*/
public Integer getSalePrice() {
return SalePrice;
}

/**
* 
* @param SalePrice
* The SalePrice
*/
public void setSalePrice(Integer SalePrice) {
this.SalePrice = SalePrice;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
