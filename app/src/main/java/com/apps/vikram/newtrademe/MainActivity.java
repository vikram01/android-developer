package com.apps.vikram.newtrademe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.apps.vikram.newtrademe.MainActivityFragment.ShowCategoryInterface;
import com.apps.vikram.newtrademe.ShowListingsFragment.ShowListingFragmentInterface;

public class MainActivity extends ActionBarActivity implements ShowCategoryInterface,ShowListingFragmentInterface {

    private static  final String LOG_TAG="MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showListings(String category) {
        ShowListingsFragment fragment = (ShowListingsFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentListing);
        if(fragment == null || (fragment!=null && !fragment.isVisible())) {
            Intent intent = new Intent(this, ShowListingsActivity.class);
            intent.putExtra("CATEGORY", category);

            Log.d(LOG_TAG, "category is ------" + category);
            startActivity(intent);
        }
        else if (fragment.isVisible())
        {
            Log.d(LOG_TAG, "category is ---Listing---" + category);
            fragment.showListings(category);
        }


    }


    //Simulating Back press for notifyDataSetChanged manipulation

    @Override
    public void onBackPressed() {
        MainActivityFragment fragment = (MainActivityFragment) getSupportFragmentManager().
                                        findFragmentById(R.id.fragmentCategory);

        if(fragment.isVisible()) {
            if (fragment.simulateBackPress())
                return;
        }
        super.onBackPressed();

        Log.d(LOG_TAG, "category On Back Presseed----------------------------");
    }

    // These are just place holders here
    @Override
    public String getCategory() {
        return null;
    }

    @Override
    public void showListingDetails(Integer listingId) {

        Log.d(LOG_TAG, "category is ---showListingDetails---" + listingId);
        Intent intent = new Intent(this,ListingDetailsActivity.class);
        intent.putExtra("LISTING_ID",listingId);
        startActivity(intent);
    }
}
