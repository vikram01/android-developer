package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class ContactDetails {

private String ContactName;
private String PhoneNumber;
private String MobilePhoneNumber;
private String BestContactTime;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The ContactName
*/
public String getContactName() {
return ContactName;
}

/**
* 
* @param ContactName
* The ContactName
*/
public void setContactName(String ContactName) {
this.ContactName = ContactName;
}

/**
* 
* @return
* The PhoneNumber
*/
public String getPhoneNumber() {
return PhoneNumber;
}

/**
* 
* @param PhoneNumber
* The PhoneNumber
*/
public void setPhoneNumber(String PhoneNumber) {
this.PhoneNumber = PhoneNumber;
}

/**
* 
* @return
* The MobilePhoneNumber
*/
public String getMobilePhoneNumber() {
return MobilePhoneNumber;
}

/**
* 
* @param MobilePhoneNumber
* The MobilePhoneNumber
*/
public void setMobilePhoneNumber(String MobilePhoneNumber) {
this.MobilePhoneNumber = MobilePhoneNumber;
}

/**
* 
* @return
* The BestContactTime
*/
public String getBestContactTime() {
return BestContactTime;
}

/**
* 
* @param BestContactTime
* The BestContactTime
*/
public void setBestContactTime(String BestContactTime) {
this.BestContactTime = BestContactTime;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}

