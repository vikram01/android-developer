package com.apps.vikram.newtrademe.Logic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apps.vikram.newtrademe.Model.Category;
import com.apps.vikram.newtrademe.R;

import java.util.ArrayList;

/**
 * Created by Vikram on 5/19/15.
 */

public class CategoryAdapter extends ArrayAdapter<Category> {
    Context mContext;

    public CategoryAdapter(Context context, ArrayList<Category> exr) {
        super(context, 0, exr);
        mContext = context;
    }

    public static String LOG_CAT = "CategoryAdapter";

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Category category = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.category_layout, parent, false);
        }
        // Lookup view for data population
        TextView tv = (TextView) convertView.findViewById(R.id.tvCategoryName);

        tv.setText(category.getName());
        // Return the completed view to render on screen
        return convertView;
    }
}

