package com.apps.vikram.newtrademe.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Questions {

private Integer TotalCount;
private Integer Page;
private Integer PageSize;
private java.util.List<QuestionsList> List = new ArrayList<QuestionsList>();
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The TotalCount
*/
public Integer getTotalCount() {
return TotalCount;
}

/**
* 
* @param TotalCount
* The TotalCount
*/
public void setTotalCount(Integer TotalCount) {
this.TotalCount = TotalCount;
}

/**
* 
* @return
* The Page
*/
public Integer getPage() {
return Page;
}

/**
* 
* @param Page
* The Page
*/
public void setPage(Integer Page) {
this.Page = Page;
}

/**
* 
* @return
* The PageSize
*/
public Integer getPageSize() {
return PageSize;
}

/**
* 
* @param PageSize
* The PageSize
*/
public void setPageSize(Integer PageSize) {
this.PageSize = PageSize;
}

/**
* 
* @return
* The List
*/
public java.util.List<QuestionsList> getList() {
return List;
}

/**
* 
* @param List
* The List
*/
public void setList(java.util.List<QuestionsList> List) {
this.List = List;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}