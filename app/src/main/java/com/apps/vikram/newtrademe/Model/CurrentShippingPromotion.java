package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class CurrentShippingPromotion {

private Integer Threshold;
private Integer ThresholdType;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The Threshold
*/
public Integer getThreshold() {
return Threshold;
}

/**
* 
* @param Threshold
* The Threshold
*/
public void setThreshold(Integer Threshold) {
this.Threshold = Threshold;
}

/**
* 
* @return
* The ThresholdType
*/
public Integer getThresholdType() {
return ThresholdType;
}

/**
* 
* @param ThresholdType
* The ThresholdType
*/
public void setThresholdType(Integer ThresholdType) {
this.ThresholdType = ThresholdType;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
