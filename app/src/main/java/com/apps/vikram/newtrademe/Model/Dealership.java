package com.apps.vikram.newtrademe.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class Dealership {

private Integer Id;
private String Name;
private String Address;
private String Suburb;
private String City;
private String PhoneNumber;
private String FaxNumber;
private String Website;
private String Logo;
private List<Dealer> Dealers = new ArrayList<Dealer>();
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The Id
*/
public Integer getId() {
return Id;
}

/**
* 
* @param Id
* The Id
*/
public void setId(Integer Id) {
this.Id = Id;
}

/**
* 
* @return
* The Name
*/
public String getName() {
return Name;
}

/**
* 
* @param Name
* The Name
*/
public void setName(String Name) {
this.Name = Name;
}

/**
* 
* @return
* The Address
*/
public String getAddress() {
return Address;
}

/**
* 
* @param Address
* The Address
*/
public void setAddress(String Address) {
this.Address = Address;
}

/**
* 
* @return
* The Suburb
*/
public String getSuburb() {
return Suburb;
}

/**
* 
* @param Suburb
* The Suburb
*/
public void setSuburb(String Suburb) {
this.Suburb = Suburb;
}

/**
* 
* @return
* The City
*/
public String getCity() {
return City;
}

/**
* 
* @param City
* The City
*/
public void setCity(String City) {
this.City = City;
}

/**
* 
* @return
* The PhoneNumber
*/
public String getPhoneNumber() {
return PhoneNumber;
}

/**
* 
* @param PhoneNumber
* The PhoneNumber
*/
public void setPhoneNumber(String PhoneNumber) {
this.PhoneNumber = PhoneNumber;
}

/**
* 
* @return
* The FaxNumber
*/
public String getFaxNumber() {
return FaxNumber;
}

/**
* 
* @param FaxNumber
* The FaxNumber
*/
public void setFaxNumber(String FaxNumber) {
this.FaxNumber = FaxNumber;
}

/**
* 
* @return
* The Website
*/
public String getWebsite() {
return Website;
}

/**
* 
* @param Website
* The Website
*/
public void setWebsite(String Website) {
this.Website = Website;
}

/**
* 
* @return
* The Logo
*/
public String getLogo() {
return Logo;
}

/**
* 
* @param Logo
* The Logo
*/
public void setLogo(String Logo) {
this.Logo = Logo;
}

/**
* 
* @return
* The Dealers
*/
public List<Dealer> getDealers() {
return Dealers;
}

/**
* 
* @param Dealers
* The Dealers
*/
public void setDealers(List<Dealer> Dealers) {
this.Dealers = Dealers;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}