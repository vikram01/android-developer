package com.apps.vikram.newtrademe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.apps.vikram.newtrademe.ShowListingsFragment.ShowListingFragmentInterface;

public class ShowListingsActivity extends ActionBarActivity implements  ShowListingFragmentInterface {

    public String category= null;
    public static final String LOG_TAG="ShowListingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_listings);

        category = getIntent().getExtras().getString("CATEGORY");

        Log.d(LOG_TAG,"category is ------"+ category);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_listing, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void showListingDetails(Integer listingId) {
        Intent intent = new Intent(this,ListingDetailsActivity.class);
        intent.putExtra("LISTING_ID",listingId);
        startActivity(intent);
    }
}
