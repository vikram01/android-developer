package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class DonationRecipient {

private Integer CharityType;
private String ImageSource;
private String Description;
private String Tagline;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The CharityType
*/
public Integer getCharityType() {
return CharityType;
}

/**
* 
* @param CharityType
* The CharityType
*/
public void setCharityType(Integer CharityType) {
this.CharityType = CharityType;
}

/**
* 
* @return
* The ImageSource
*/
public String getImageSource() {
return ImageSource;
}

/**
* 
* @param ImageSource
* The ImageSource
*/
public void setImageSource(String ImageSource) {
this.ImageSource = ImageSource;
}

/**
* 
* @return
* The Description
*/
public String getDescription() {
return Description;
}

/**
* 
* @param Description
* The Description
*/
public void setDescription(String Description) {
this.Description = Description;
}

/**
* 
* @return
* The Tagline
*/
public String getTagline() {
return Tagline;
}

/**
* 
* @param Tagline
* The Tagline
*/
public void setTagline(String Tagline) {
this.Tagline = Tagline;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
