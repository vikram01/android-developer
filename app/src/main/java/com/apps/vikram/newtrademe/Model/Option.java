package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vikram on 5/19/15.
 */
public class Option {

    private String Value;
    private String Display;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The Value
     */
    public String getValue() {
        return Value;
    }

    /**
     *
     * @param Value
     * The Value
     */
    public void setValue(String Value) {
        this.Value = Value;
    }

    /**
     *
     * @return
     * The Display
     */
    public String getDisplay() {
        return Display;
    }

    /**
     *
     * @param Display
     * The Display
     */
    public void setDisplay(String Display) {
        this.Display = Display;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}