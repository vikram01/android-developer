package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class Dealer {

private String FullName;
private String Position;
private String MobilePhoneNumber;
private String OfficePhoneNumber;
private String EMail;
private String FaxNumber;
private String HomePhoneNumber;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The FullName
*/
public String getFullName() {
return FullName;
}

/**
* 
* @param FullName
* The FullName
*/
public void setFullName(String FullName) {
this.FullName = FullName;
}

/**
* 
* @return
* The Position
*/
public String getPosition() {
return Position;
}

/**
* 
* @param Position
* The Position
*/
public void setPosition(String Position) {
this.Position = Position;
}

/**
* 
* @return
* The MobilePhoneNumber
*/
public String getMobilePhoneNumber() {
return MobilePhoneNumber;
}

/**
* 
* @param MobilePhoneNumber
* The MobilePhoneNumber
*/
public void setMobilePhoneNumber(String MobilePhoneNumber) {
this.MobilePhoneNumber = MobilePhoneNumber;
}

/**
* 
* @return
* The OfficePhoneNumber
*/
public String getOfficePhoneNumber() {
return OfficePhoneNumber;
}

/**
* 
* @param OfficePhoneNumber
* The OfficePhoneNumber
*/
public void setOfficePhoneNumber(String OfficePhoneNumber) {
this.OfficePhoneNumber = OfficePhoneNumber;
}

/**
* 
* @return
* The EMail
*/
public String getEMail() {
return EMail;
}

/**
* 
* @param EMail
* The EMail
*/
public void setEMail(String EMail) {
this.EMail = EMail;
}

/**
* 
* @return
* The FaxNumber
*/
public String getFaxNumber() {
return FaxNumber;
}

/**
* 
* @param FaxNumber
* The FaxNumber
*/
public void setFaxNumber(String FaxNumber) {
this.FaxNumber = FaxNumber;
}

/**
* 
* @return
* The HomePhoneNumber
*/
public String getHomePhoneNumber() {
return HomePhoneNumber;
}

/**
* 
* @param HomePhoneNumber
* The HomePhoneNumber
*/
public void setHomePhoneNumber(String HomePhoneNumber) {
this.HomePhoneNumber = HomePhoneNumber;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
