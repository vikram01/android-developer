package com.apps.vikram.newtrademe.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class LocalSales {

private List<RecentSale> RecentSales = new ArrayList<RecentSale>();
private String SalesRegionName;
private Integer AveragePrice;
private String LinkToQV;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The RecentSales
*/
public List<RecentSale> getRecentSales() {
return RecentSales;
}

/**
* 
* @param RecentSales
* The RecentSales
*/
public void setRecentSales(List<RecentSale> RecentSales) {
this.RecentSales = RecentSales;
}

/**
* 
* @return
* The SalesRegionName
*/
public String getSalesRegionName() {
return SalesRegionName;
}

/**
* 
* @param SalesRegionName
* The SalesRegionName
*/
public void setSalesRegionName(String SalesRegionName) {
this.SalesRegionName = SalesRegionName;
}

/**
* 
* @return
* The AveragePrice
*/
public Integer getAveragePrice() {
return AveragePrice;
}

/**
* 
* @param AveragePrice
* The AveragePrice
*/
public void setAveragePrice(Integer AveragePrice) {
this.AveragePrice = AveragePrice;
}

/**
* 
* @return
* The LinkToQV
*/
public String getLinkToQV() {
return LinkToQV;
}

/**
* 
* @param LinkToQV
* The LinkToQV
*/
public void setLinkToQV(String LinkToQV) {
this.LinkToQV = LinkToQV;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}