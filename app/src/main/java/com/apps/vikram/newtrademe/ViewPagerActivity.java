package com.apps.vikram.newtrademe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.apps.vikram.newtrademe.MainActivityFragment.ShowCategoryInterface;
import com.apps.vikram.newtrademe.ShowListingsFragment.ShowListingFragmentInterface;
import com.astuetz.PagerSlidingTabStrip;

import java.util.List;
import java.util.Vector;


public class ViewPagerActivity extends ActionBarActivity  implements ShowCategoryInterface ,ShowListingFragmentInterface {

    private static final String LOG_TAG="ViewPagerActivity";
    private static final int NUM_ITEMS=2;

    ViewPager mPager;
    static PagerAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);
        mPager = (ViewPager)findViewById( R.id.pager );
        if(mPager!= null)
            initialisePaging();


    }


    private void initialisePaging() {

        Log.d(LOG_TAG, "Inside - initialisePaging");
        List<Fragment> fragments = new Vector<>();
        fragments.add(Fragment.instantiate(this, MainActivityFragment.class.getName()));
        fragments.add(Fragment.instantiate(this, ShowListingsFragment.class.getName()));

        this.adapter  = new PagerAdapter(super.getSupportFragmentManager(), fragments);
        //
        mPager = (ViewPager)super.findViewById(R.id.pager);
        mPager.setAdapter(this.adapter);
        // Give the PagerSlidingTabStrip the ViewPager
        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(mPager);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_pager, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showListings(String category) {

        Log.d(LOG_TAG, "Inside - showListings" + category);
        ShowListingsFragment fragment;
        if(mPager!=null && mPager.getVisibility()== View.VISIBLE )
            fragment = (ShowListingsFragment)adapter.getItem(1);
        else
            fragment = (ShowListingsFragment)getSupportFragmentManager().findFragmentById(R.id.fragmentListing);
        if(fragment!=null)
            fragment.showListings(category);

    }

    @Override
    public String getCategory() {
        return null;
    }

    @Override
    public void showListingDetails(Integer listingId) {
        Log.d(LOG_TAG, "category is ---showListingDetails---" + listingId);
        Intent intent = new Intent(this,ListingDetailsActivity.class);
        intent.putExtra("LISTING_ID", listingId);
        startActivity(intent);
    }


    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        //super.onSaveInstanceState(outState);
    }
    @Override
    public void onBackPressed() {

        if(mPager!= null && mPager.getCurrentItem() ==1) // Listing
        {
            mPager.setCurrentItem(0);
            return;
        }
        else
        {
            MainActivityFragment fragment;
            if(adapter!=null)
                fragment = (MainActivityFragment) adapter.getItem(0);
            else
                fragment = (MainActivityFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentCategory);
            if(fragment.isVisible()) {
                if (fragment.simulateBackPress())
                    return;
            }
        }
        super.onBackPressed();
        Log.d(LOG_TAG, "category On Back Presseed----------------------------");
    }


    public static class PagerAdapter extends FragmentPagerAdapter {
        private final String tabTitles[] = new String[]{"Categories", "Listing"};
        private List<Fragment> fragments;

        public PagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments= fragments;
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }

    }




}
