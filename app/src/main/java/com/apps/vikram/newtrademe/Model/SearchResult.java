package com.apps.vikram.newtrademe.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vikram on 5/19/15.
 */
public class SearchResult {

    private Integer TotalCount;
    private Integer Page;
    private Integer PageSize;
    private java.util.List<com.apps.vikram.newtrademe.Model.List> List =
                        new ArrayList<com.apps.vikram.newtrademe.Model.List>();
    private String DidYouMean;
    private java.util.List<FoundCategory> FoundCategories = new ArrayList<FoundCategory>();
    private Integer FavouriteId;
    private Integer FavouriteType;
    private java.util.List<Parameter> Parameters = new ArrayList<Parameter>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The TotalCount
     */
    public Integer getTotalCount() {
        return TotalCount;
    }

    /**
     *
     * @param TotalCount
     * The TotalCount
     */
    public void setTotalCount(Integer TotalCount) {
        this.TotalCount = TotalCount;
    }

    /**
     *
     * @return
     * The Page
     */
    public Integer getPage() {
        return Page;
    }

    /**
     *
     * @param Page
     * The Page
     */
    public void setPage(Integer Page) {
        this.Page = Page;
    }

    /**
     *
     * @return
     * The PageSize
     */
    public Integer getPageSize() {
        return PageSize;
    }

    /**
     *
     * @param PageSize
     * The PageSize
     */
    public void setPageSize(Integer PageSize) {
        this.PageSize = PageSize;
    }

    /**
     *
     * @return
     * The List
     */
    public java.util.List<com.apps.vikram.newtrademe.Model.List> getList() {
        return List;
    }

    /**
     *
     * @param List
     * The List
     */
    public void setList(java.util.List<com.apps.vikram.newtrademe.Model.List> List) {
        this.List = List;
    }

    /**
     *
     * @return
     * The DidYouMean
     */
    public String getDidYouMean() {
        return DidYouMean;
    }

    /**
     *
     * @param DidYouMean
     * The DidYouMean
     */
    public void setDidYouMean(String DidYouMean) {
        this.DidYouMean = DidYouMean;
    }

    /**
     *
     * @return
     * The FoundCategories
     */
    public java.util.List<FoundCategory> getFoundCategories() {
        return FoundCategories;
    }

    /**
     *
     * @param FoundCategories
     * The FoundCategories
     */
    public void setFoundCategories(java.util.List<FoundCategory> FoundCategories) {
        this.FoundCategories = FoundCategories;
    }

    /**
     *
     * @return
     * The FavouriteId
     */
    public Integer getFavouriteId() {
        return FavouriteId;
    }

    /**
     *
     * @param FavouriteId
     * The FavouriteId
     */
    public void setFavouriteId(Integer FavouriteId) {
        this.FavouriteId = FavouriteId;
    }

    /**
     *
     * @return
     * The FavouriteType
     */
    public Integer getFavouriteType() {
        return FavouriteType;
    }

    /**
     *
     * @param FavouriteType
     * The FavouriteType
     */
    public void setFavouriteType(Integer FavouriteType) {
        this.FavouriteType = FavouriteType;
    }

    /**
     *
     * @return
     * The Parameters
     */
    public java.util.List<Parameter> getParameters() {
        return Parameters;
    }

    /**
     *
     * @param Parameters
     * The Parameters
     */
    public void setParameters(java.util.List<Parameter> Parameters) {
        this.Parameters = Parameters;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}