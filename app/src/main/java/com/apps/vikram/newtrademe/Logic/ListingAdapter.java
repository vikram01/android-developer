package com.apps.vikram.newtrademe.Logic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.vikram.newtrademe.Model.List;
import com.apps.vikram.newtrademe.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Vikram on 5/19/15.
 */

public class ListingAdapter extends ArrayAdapter<List> {
    Context mContext;

    public ListingAdapter(Context context, ArrayList<List> exr) {
        super(context, 0, exr);
        mContext = context;
    }

    public static String LOG_CAT = "ListAdapter";

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        List listing = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listing_layout, parent, false);
        }
        // Lookup view for data population
        TextView tvListingTitle = (TextView) convertView.findViewById(R.id.tvListingTitle);
        TextView tvListingId = (TextView) convertView.findViewById(R.id.tvListingId);
        TextView tvListingRegion = (TextView) convertView.findViewById(R.id.tvListingRegion);
        TextView tvListingPrice = (TextView) convertView.findViewById(R.id.tvListingPrice);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imgViewListings);

        tvListingTitle.setText(listing.getTitle());
        tvListingId.setText(listing.getListingId().toString());
        tvListingPrice.setText(listing.getPriceDisplay());
        tvListingRegion.setText(listing.getRegion());

        Picasso.with(mContext).load(listing.getPictureHref()).into(imageView);


        // Return the completed view to render on screen
        return convertView;
    }
}

