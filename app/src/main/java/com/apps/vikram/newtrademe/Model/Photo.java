package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class Photo {

private Integer PhotoId;
private com.apps.vikram.newtrademe.Model.Value Value;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The PhotoId
*/
public Integer getPhotoId() {
return PhotoId;
}

/**
* 
* @param PhotoId
* The PhotoId
*/
public void setPhotoId(Integer PhotoId) {
this.PhotoId = PhotoId;
}

/**
* 
* @return
* The Value
*/
public com.apps.vikram.newtrademe.Model.Value getValue() {
return Value;
}

/**
* 
* @param Value
* The Value
*/
public void setValue(com.apps.vikram.newtrademe.Model.Value Value) {
this.Value = Value;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
