package com.apps.vikram.newtrademe.ApiInterface;

import com.apps.vikram.newtrademe.Model.Category;
import com.apps.vikram.newtrademe.Model.ListingDetail;
import com.apps.vikram.newtrademe.Model.SearchResult;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Vikram on 5/19/15.
 */
public interface TrademeApiInterface {

    //This service will returns categories for the specified category number
    @GET("/Categories{categoryNumber}.json")
    void getSubCategories(@Path("categoryNumber") String categoryNumber, Callback<Category> cb);


    //get listings for a category
    @GET("/Search/General.json")
    void listingSearch(@Query("category") String categoryNumber,
                       @Query("rows")String rows,
                       @Query("photo_size")String photoSize,
                       Callback<SearchResult> cb);
    //get Listing Detail
    @GET("/Listings/{listingId}.json")
    void listingDetail(@Path("listingId") Integer listingId,Callback<ListingDetail> cb);


}
