package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vikram on 5/19/15.
 */
public class OpenHome {

    private String Start;
    private String End;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The Start
     */
    public String getStart() {
        return Start;
    }

    /**
     *
     * @param Start
     * The Start
     */
    public void setStart(String Start) {
        this.Start = Start;
    }

    /**
     *
     * @return
     * The End
     */
    public String getEnd() {
        return End;
    }

    /**
     *
     * @param End
     * The End
     */
    public void setEnd(String End) {
        this.End = End;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}