package com.apps.vikram.newtrademe.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class ListingDetail {

private Integer ListingId;
private String Title;
private String Category;
private Double StartPrice;
private Double BuyNowPrice;
private String StartDate;
private String EndDate;
private Boolean IsFeatured;
private Boolean HasGallery;
private Boolean IsBold;
private Boolean IsHighlighted;
private Boolean HasHomePageFeature;
private Double MaxBidAmount;
private String AsAt;
private String CategoryPath;
private Integer PhotoId;
private Boolean HasPayNow;
private Boolean IsNew;
private Integer RegionId;
private String Region;
private String Suburb;
private Integer BidCount;
private Integer ViewCount;
private Boolean IsReserveMet;
private Boolean HasReserve;
private Boolean HasBuyNow;
private String NoteDate;
private String CategoryName;
private Integer ReserveState;
private List<Attribute> Attributes = new ArrayList<Attribute>();
private Boolean IsClassified;
private List<OpenHome> OpenHomes = new ArrayList<OpenHome>();
private Integer RelistedItemId;
private String Subtitle;
private Double ReservePrice;
private Boolean IsBuyNowOnly;
private Boolean HasMultiple;
private Integer Quantity;
private Boolean IsFlatShippingCharge;
private Integer RemainingGalleryPlusRelists;
private Double MinimumNextBidAmount;
private Boolean IsOnWatchList;
private com.apps.vikram.newtrademe.Model.GeographicLocation GeographicLocation;
private String PriceDisplay;
private Integer TotalReviewCount;
private Integer PositiveReviewCount;
private Boolean SendPaymentInstructions;
private Boolean CanUsePayNowInstant;
private String ExternalReferenceId;
private com.apps.vikram.newtrademe.Model.LocalSales LocalSales;
private String SKU;
private com.apps.vikram.newtrademe.Model.Member Member;
private String Body;
private com.apps.vikram.newtrademe.Model.Bids Bids;
private QuestionsList Questions;
private List<Photo> Photos = new ArrayList<Photo>();
private Integer AllowsPickups;
private List<ShippingOption> ShippingOptions = new ArrayList<ShippingOption>();
private String PaymentOptions;
private com.apps.vikram.newtrademe.Model.Dealership Dealership;
private com.apps.vikram.newtrademe.Model.Agency Agency;
private com.apps.vikram.newtrademe.Model.ContactDetails ContactDetails;
private Boolean IsOrNearOffer;
private Integer UnansweredQuestionCount;
private Boolean AuthenticatedMembersOnly;
private List<Sale> Sales = new ArrayList<Sale>();
private Integer OfferStatus;
private com.apps.vikram.newtrademe.Model.PendingOffer PendingOffer;
private com.apps.vikram.newtrademe.Model.ClosedOffer ClosedOffer;
private Boolean FirearmsLicenseRequiredToBuy;
private Boolean Over18DeclarationRequiredToBuy;
private Boolean CanOffer;
private Boolean CanRelist;
private Boolean WithdrawnBySeller;
private com.apps.vikram.newtrademe.Model.DonationRecipient DonationRecipient;
private Boolean IsInTradeProtected;
private Boolean IsInCart;
private com.apps.vikram.newtrademe.Model.CurrentShippingPromotion CurrentShippingPromotion;
private Boolean IsLeading;
private Boolean IsOutbid;
private Boolean CanAddToCart;
private Integer CartItemId;
private com.apps.vikram.newtrademe.Model.MemberProfile MemberProfile;
private Boolean ViewingTrackerSupported;
private Double CurrentAutoBid;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The ListingId
*/
public Integer getListingId() {
return ListingId;
}

/**
* 
* @param ListingId
* The ListingId
*/
public void setListingId(Integer ListingId) {
this.ListingId = ListingId;
}

/**
* 
* @return
* The Title
*/
public String getTitle() {
return Title;
}

/**
* 
* @param Title
* The Title
*/
public void setTitle(String Title) {
this.Title = Title;
}

/**
* 
* @return
* The Category
*/
public String getCategory() {
return Category;
}

/**
* 
* @param Category
* The Category
*/
public void setCategory(String Category) {
this.Category = Category;
}

/**
* 
* @return
* The StartPrice
*/
public Double getStartPrice() {
return StartPrice;
}

/**
* 
* @param StartPrice
* The StartPrice
*/
public void setStartPrice(Double StartPrice) {
this.StartPrice = StartPrice;
}

/**
* 
* @return
* The BuyNowPrice
*/
public Double getBuyNowPrice() {
return BuyNowPrice;
}

/**
* 
* @param BuyNowPrice
* The BuyNowPrice
*/
public void setBuyNowPrice(Double BuyNowPrice) {
this.BuyNowPrice = BuyNowPrice;
}

/**
* 
* @return
* The StartDate
*/
public String getStartDate() {
return StartDate;
}

/**
* 
* @param StartDate
* The StartDate
*/
public void setStartDate(String StartDate) {
this.StartDate = StartDate;
}

/**
* 
* @return
* The EndDate
*/
public String getEndDate() {
return EndDate;
}

/**
* 
* @param EndDate
* The EndDate
*/
public void setEndDate(String EndDate) {
this.EndDate = EndDate;
}

/**
* 
* @return
* The IsFeatured
*/
public Boolean getIsFeatured() {
return IsFeatured;
}

/**
* 
* @param IsFeatured
* The IsFeatured
*/
public void setIsFeatured(Boolean IsFeatured) {
this.IsFeatured = IsFeatured;
}

/**
* 
* @return
* The HasGallery
*/
public Boolean getHasGallery() {
return HasGallery;
}

/**
* 
* @param HasGallery
* The HasGallery
*/
public void setHasGallery(Boolean HasGallery) {
this.HasGallery = HasGallery;
}

/**
* 
* @return
* The IsBold
*/
public Boolean getIsBold() {
return IsBold;
}

/**
* 
* @param IsBold
* The IsBold
*/
public void setIsBold(Boolean IsBold) {
this.IsBold = IsBold;
}

/**
* 
* @return
* The IsHighlighted
*/
public Boolean getIsHighlighted() {
return IsHighlighted;
}

/**
* 
* @param IsHighlighted
* The IsHighlighted
*/
public void setIsHighlighted(Boolean IsHighlighted) {
this.IsHighlighted = IsHighlighted;
}

/**
* 
* @return
* The HasHomePageFeature
*/
public Boolean getHasHomePageFeature() {
return HasHomePageFeature;
}

/**
* 
* @param HasHomePageFeature
* The HasHomePageFeature
*/
public void setHasHomePageFeature(Boolean HasHomePageFeature) {
this.HasHomePageFeature = HasHomePageFeature;
}

/**
* 
* @return
* The MaxBidAmount
*/
public Double getMaxBidAmount() {
return MaxBidAmount;
}

/**
* 
* @param MaxBidAmount
* The MaxBidAmount
*/
public void setMaxBidAmount(Double MaxBidAmount) {
this.MaxBidAmount = MaxBidAmount;
}

/**
* 
* @return
* The AsAt
*/
public String getAsAt() {
return AsAt;
}

/**
* 
* @param AsAt
* The AsAt
*/
public void setAsAt(String AsAt) {
this.AsAt = AsAt;
}

/**
* 
* @return
* The CategoryPath
*/
public String getCategoryPath() {
return CategoryPath;
}

/**
* 
* @param CategoryPath
* The CategoryPath
*/
public void setCategoryPath(String CategoryPath) {
this.CategoryPath = CategoryPath;
}

/**
* 
* @return
* The PhotoId
*/
public Integer getPhotoId() {
return PhotoId;
}

/**
* 
* @param PhotoId
* The PhotoId
*/
public void setPhotoId(Integer PhotoId) {
this.PhotoId = PhotoId;
}

/**
* 
* @return
* The HasPayNow
*/
public Boolean getHasPayNow() {
return HasPayNow;
}

/**
* 
* @param HasPayNow
* The HasPayNow
*/
public void setHasPayNow(Boolean HasPayNow) {
this.HasPayNow = HasPayNow;
}

/**
* 
* @return
* The IsNew
*/
public Boolean getIsNew() {
return IsNew;
}

/**
* 
* @param IsNew
* The IsNew
*/
public void setIsNew(Boolean IsNew) {
this.IsNew = IsNew;
}

/**
* 
* @return
* The RegionId
*/
public Integer getRegionId() {
return RegionId;
}

/**
* 
* @param RegionId
* The RegionId
*/
public void setRegionId(Integer RegionId) {
this.RegionId = RegionId;
}

/**
* 
* @return
* The Region
*/
public String getRegion() {
return Region;
}

/**
* 
* @param Region
* The Region
*/
public void setRegion(String Region) {
this.Region = Region;
}

/**
* 
* @return
* The Suburb
*/
public String getSuburb() {
return Suburb;
}

/**
* 
* @param Suburb
* The Suburb
*/
public void setSuburb(String Suburb) {
this.Suburb = Suburb;
}

/**
* 
* @return
* The BidCount
*/
public Integer getBidCount() {
return BidCount;
}

/**
* 
* @param BidCount
* The BidCount
*/
public void setBidCount(Integer BidCount) {
this.BidCount = BidCount;
}

/**
* 
* @return
* The ViewCount
*/
public Integer getViewCount() {
return ViewCount;
}

/**
* 
* @param ViewCount
* The ViewCount
*/
public void setViewCount(Integer ViewCount) {
this.ViewCount = ViewCount;
}

/**
* 
* @return
* The IsReserveMet
*/
public Boolean getIsReserveMet() {
return IsReserveMet;
}

/**
* 
* @param IsReserveMet
* The IsReserveMet
*/
public void setIsReserveMet(Boolean IsReserveMet) {
this.IsReserveMet = IsReserveMet;
}

/**
* 
* @return
* The HasReserve
*/
public Boolean getHasReserve() {
return HasReserve;
}

/**
* 
* @param HasReserve
* The HasReserve
*/
public void setHasReserve(Boolean HasReserve) {
this.HasReserve = HasReserve;
}

/**
* 
* @return
* The HasBuyNow
*/
public Boolean getHasBuyNow() {
return HasBuyNow;
}

/**
* 
* @param HasBuyNow
* The HasBuyNow
*/
public void setHasBuyNow(Boolean HasBuyNow) {
this.HasBuyNow = HasBuyNow;
}

/**
* 
* @return
* The NoteDate
*/
public String getNoteDate() {
return NoteDate;
}

/**
* 
* @param NoteDate
* The NoteDate
*/
public void setNoteDate(String NoteDate) {
this.NoteDate = NoteDate;
}

/**
* 
* @return
* The CategoryName
*/
public String getCategoryName() {
return CategoryName;
}

/**
* 
* @param CategoryName
* The CategoryName
*/
public void setCategoryName(String CategoryName) {
this.CategoryName = CategoryName;
}

/**
* 
* @return
* The ReserveState
*/
public Integer getReserveState() {
return ReserveState;
}

/**
* 
* @param ReserveState
* The ReserveState
*/
public void setReserveState(Integer ReserveState) {
this.ReserveState = ReserveState;
}

/**
* 
* @return
* The Attributes
*/
public List<Attribute> getAttributes() {
return Attributes;
}

/**
* 
* @param Attributes
* The Attributes
*/
public void setAttributes(List<Attribute> Attributes) {
this.Attributes = Attributes;
}

/**
* 
* @return
* The IsClassified
*/
public Boolean getIsClassified() {
return IsClassified;
}

/**
* 
* @param IsClassified
* The IsClassified
*/
public void setIsClassified(Boolean IsClassified) {
this.IsClassified = IsClassified;
}

/**
* 
* @return
* The OpenHomes
*/
public List<OpenHome> getOpenHomes() {
return OpenHomes;
}

/**
* 
* @param OpenHomes
* The OpenHomes
*/
public void setOpenHomes(List<OpenHome> OpenHomes) {
this.OpenHomes = OpenHomes;
}

/**
* 
* @return
* The RelistedItemId
*/
public Integer getRelistedItemId() {
return RelistedItemId;
}

/**
* 
* @param RelistedItemId
* The RelistedItemId
*/
public void setRelistedItemId(Integer RelistedItemId) {
this.RelistedItemId = RelistedItemId;
}

/**
* 
* @return
* The Subtitle
*/
public String getSubtitle() {
return Subtitle;
}

/**
* 
* @param Subtitle
* The Subtitle
*/
public void setSubtitle(String Subtitle) {
this.Subtitle = Subtitle;
}

/**
* 
* @return
* The ReservePrice
*/
public Double getReservePrice() {
return ReservePrice;
}

/**
* 
* @param ReservePrice
* The ReservePrice
*/
public void setReservePrice(Double ReservePrice) {
this.ReservePrice = ReservePrice;
}

/**
* 
* @return
* The IsBuyNowOnly
*/
public Boolean getIsBuyNowOnly() {
return IsBuyNowOnly;
}

/**
* 
* @param IsBuyNowOnly
* The IsBuyNowOnly
*/
public void setIsBuyNowOnly(Boolean IsBuyNowOnly) {
this.IsBuyNowOnly = IsBuyNowOnly;
}

/**
* 
* @return
* The HasMultiple
*/
public Boolean getHasMultiple() {
return HasMultiple;
}

/**
* 
* @param HasMultiple
* The HasMultiple
*/
public void setHasMultiple(Boolean HasMultiple) {
this.HasMultiple = HasMultiple;
}

/**
* 
* @return
* The Quantity
*/
public Integer getQuantity() {
return Quantity;
}

/**
* 
* @param Quantity
* The Quantity
*/
public void setQuantity(Integer Quantity) {
this.Quantity = Quantity;
}

/**
* 
* @return
* The IsFlatShippingCharge
*/
public Boolean getIsFlatShippingCharge() {
return IsFlatShippingCharge;
}

/**
* 
* @param IsFlatShippingCharge
* The IsFlatShippingCharge
*/
public void setIsFlatShippingCharge(Boolean IsFlatShippingCharge) {
this.IsFlatShippingCharge = IsFlatShippingCharge;
}

/**
* 
* @return
* The RemainingGalleryPlusRelists
*/
public Integer getRemainingGalleryPlusRelists() {
return RemainingGalleryPlusRelists;
}

/**
* 
* @param RemainingGalleryPlusRelists
* The RemainingGalleryPlusRelists
*/
public void setRemainingGalleryPlusRelists(Integer RemainingGalleryPlusRelists) {
this.RemainingGalleryPlusRelists = RemainingGalleryPlusRelists;
}

/**
* 
* @return
* The MinimumNextBidAmount
*/
public Double getMinimumNextBidAmount() {
return MinimumNextBidAmount;
}

/**
* 
* @param MinimumNextBidAmount
* The MinimumNextBidAmount
*/
public void setMinimumNextBidAmount(Double MinimumNextBidAmount) {
this.MinimumNextBidAmount = MinimumNextBidAmount;
}

/**
* 
* @return
* The IsOnWatchList
*/
public Boolean getIsOnWatchList() {
return IsOnWatchList;
}

/**
* 
* @param IsOnWatchList
* The IsOnWatchList
*/
public void setIsOnWatchList(Boolean IsOnWatchList) {
this.IsOnWatchList = IsOnWatchList;
}

/**
* 
* @return
* The GeographicLocation
*/
public com.apps.vikram.newtrademe.Model.GeographicLocation getGeographicLocation() {
return GeographicLocation;
}

/**
* 
* @param GeographicLocation
* The GeographicLocation
*/
public void setGeographicLocation(com.apps.vikram.newtrademe.Model.GeographicLocation GeographicLocation) {
this.GeographicLocation = GeographicLocation;
}

/**
* 
* @return
* The PriceDisplay
*/
public String getPriceDisplay() {
return PriceDisplay;
}

/**
* 
* @param PriceDisplay
* The PriceDisplay
*/
public void setPriceDisplay(String PriceDisplay) {
this.PriceDisplay = PriceDisplay;
}

/**
* 
* @return
* The TotalReviewCount
*/
public Integer getTotalReviewCount() {
return TotalReviewCount;
}

/**
* 
* @param TotalReviewCount
* The TotalReviewCount
*/
public void setTotalReviewCount(Integer TotalReviewCount) {
this.TotalReviewCount = TotalReviewCount;
}

/**
* 
* @return
* The PositiveReviewCount
*/
public Integer getPositiveReviewCount() {
return PositiveReviewCount;
}

/**
* 
* @param PositiveReviewCount
* The PositiveReviewCount
*/
public void setPositiveReviewCount(Integer PositiveReviewCount) {
this.PositiveReviewCount = PositiveReviewCount;
}

/**
* 
* @return
* The SendPaymentInstructions
*/
public Boolean getSendPaymentInstructions() {
return SendPaymentInstructions;
}

/**
* 
* @param SendPaymentInstructions
* The SendPaymentInstructions
*/
public void setSendPaymentInstructions(Boolean SendPaymentInstructions) {
this.SendPaymentInstructions = SendPaymentInstructions;
}

/**
* 
* @return
* The CanUsePayNowInstant
*/
public Boolean getCanUsePayNowInstant() {
return CanUsePayNowInstant;
}

/**
* 
* @param CanUsePayNowInstant
* The CanUsePayNowInstant
*/
public void setCanUsePayNowInstant(Boolean CanUsePayNowInstant) {
this.CanUsePayNowInstant = CanUsePayNowInstant;
}

/**
* 
* @return
* The ExternalReferenceId
*/
public String getExternalReferenceId() {
return ExternalReferenceId;
}

/**
* 
* @param ExternalReferenceId
* The ExternalReferenceId
*/
public void setExternalReferenceId(String ExternalReferenceId) {
this.ExternalReferenceId = ExternalReferenceId;
}

/**
* 
* @return
* The LocalSales
*/
public com.apps.vikram.newtrademe.Model.LocalSales getLocalSales() {
return LocalSales;
}

/**
* 
* @param LocalSales
* The LocalSales
*/
public void setLocalSales(com.apps.vikram.newtrademe.Model.LocalSales LocalSales) {
this.LocalSales = LocalSales;
}

/**
* 
* @return
* The SKU
*/
public String getSKU() {
return SKU;
}

/**
* 
* @param SKU
* The SKU
*/
public void setSKU(String SKU) {
this.SKU = SKU;
}

/**
* 
* @return
* The Member
*/
public com.apps.vikram.newtrademe.Model.Member getMember() {
return Member;
}

/**
* 
* @param Member
* The Member
*/
public void setMember(com.apps.vikram.newtrademe.Model.Member Member) {
this.Member = Member;
}

/**
* 
* @return
* The Body
*/
public String getBody() {
return Body;
}

/**
* 
* @param Body
* The Body
*/
public void setBody(String Body) {
this.Body = Body;
}

/**
* 
* @return
* The Bids
*/
public com.apps.vikram.newtrademe.Model.Bids getBids() {
return Bids;
}

/**
* 
* @param Bids
* The Bids
*/
public void setBids(com.apps.vikram.newtrademe.Model.Bids Bids) {
this.Bids = Bids;
}

/**
* 
* @return
* The Questions
*/
public QuestionsList getQuestions() {
return Questions;
}

/**
* 
* @param Questions
* The Questions
*/
public void setQuestions(QuestionsList Questions) {
this.Questions = Questions;
}

/**
* 
* @return
* The Photos
*/
public List<Photo> getPhotos() {
return Photos;
}

/**
* 
* @param Photos
* The Photos
*/
public void setPhotos(List<Photo> Photos) {
this.Photos = Photos;
}

/**
* 
* @return
* The AllowsPickups
*/
public Integer getAllowsPickups() {
return AllowsPickups;
}

/**
* 
* @param AllowsPickups
* The AllowsPickups
*/
public void setAllowsPickups(Integer AllowsPickups) {
this.AllowsPickups = AllowsPickups;
}

/**
* 
* @return
* The ShippingOptions
*/
public List<ShippingOption> getShippingOptions() {
return ShippingOptions;
}

/**
* 
* @param ShippingOptions
* The ShippingOptions
*/
public void setShippingOptions(List<ShippingOption> ShippingOptions) {
this.ShippingOptions = ShippingOptions;
}

/**
* 
* @return
* The PaymentOptions
*/
public String getPaymentOptions() {
return PaymentOptions;
}

/**
* 
* @param PaymentOptions
* The PaymentOptions
*/
public void setPaymentOptions(String PaymentOptions) {
this.PaymentOptions = PaymentOptions;
}

/**
* 
* @return
* The Dealership
*/
public com.apps.vikram.newtrademe.Model.Dealership getDealership() {
return Dealership;
}

/**
* 
* @param Dealership
* The Dealership
*/
public void setDealership(com.apps.vikram.newtrademe.Model.Dealership Dealership) {
this.Dealership = Dealership;
}

/**
* 
* @return
* The Agency
*/
public com.apps.vikram.newtrademe.Model.Agency getAgency() {
return Agency;
}

/**
* 
* @param Agency
* The Agency
*/
public void setAgency(com.apps.vikram.newtrademe.Model.Agency Agency) {
this.Agency = Agency;
}

/**
* 
* @return
* The ContactDetails
*/
public com.apps.vikram.newtrademe.Model.ContactDetails getContactDetails() {
return ContactDetails;
}

/**
* 
* @param ContactDetails
* The ContactDetails
*/
public void setContactDetails(com.apps.vikram.newtrademe.Model.ContactDetails ContactDetails) {
this.ContactDetails = ContactDetails;
}

/**
* 
* @return
* The IsOrNearOffer
*/
public Boolean getIsOrNearOffer() {
return IsOrNearOffer;
}

/**
* 
* @param IsOrNearOffer
* The IsOrNearOffer
*/
public void setIsOrNearOffer(Boolean IsOrNearOffer) {
this.IsOrNearOffer = IsOrNearOffer;
}

/**
* 
* @return
* The UnansweredQuestionCount
*/
public Integer getUnansweredQuestionCount() {
return UnansweredQuestionCount;
}

/**
* 
* @param UnansweredQuestionCount
* The UnansweredQuestionCount
*/
public void setUnansweredQuestionCount(Integer UnansweredQuestionCount) {
this.UnansweredQuestionCount = UnansweredQuestionCount;
}

/**
* 
* @return
* The AuthenticatedMembersOnly
*/
public Boolean getAuthenticatedMembersOnly() {
return AuthenticatedMembersOnly;
}

/**
* 
* @param AuthenticatedMembersOnly
* The AuthenticatedMembersOnly
*/
public void setAuthenticatedMembersOnly(Boolean AuthenticatedMembersOnly) {
this.AuthenticatedMembersOnly = AuthenticatedMembersOnly;
}

/**
* 
* @return
* The Sales
*/
public List<Sale> getSales() {
return Sales;
}

/**
* 
* @param Sales
* The Sales
*/
public void setSales(List<Sale> Sales) {
this.Sales = Sales;
}

/**
* 
* @return
* The OfferStatus
*/
public Integer getOfferStatus() {
return OfferStatus;
}

/**
* 
* @param OfferStatus
* The OfferStatus
*/
public void setOfferStatus(Integer OfferStatus) {
this.OfferStatus = OfferStatus;
}

/**
* 
* @return
* The PendingOffer
*/
public com.apps.vikram.newtrademe.Model.PendingOffer getPendingOffer() {
return PendingOffer;
}

/**
* 
* @param PendingOffer
* The PendingOffer
*/
public void setPendingOffer(com.apps.vikram.newtrademe.Model.PendingOffer PendingOffer) {
this.PendingOffer = PendingOffer;
}

/**
* 
* @return
* The ClosedOffer
*/
public com.apps.vikram.newtrademe.Model.ClosedOffer getClosedOffer() {
return ClosedOffer;
}

/**
* 
* @param ClosedOffer
* The ClosedOffer
*/
public void setClosedOffer(com.apps.vikram.newtrademe.Model.ClosedOffer ClosedOffer) {
this.ClosedOffer = ClosedOffer;
}

/**
* 
* @return
* The FirearmsLicenseRequiredToBuy
*/
public Boolean getFirearmsLicenseRequiredToBuy() {
return FirearmsLicenseRequiredToBuy;
}

/**
* 
* @param FirearmsLicenseRequiredToBuy
* The FirearmsLicenseRequiredToBuy
*/
public void setFirearmsLicenseRequiredToBuy(Boolean FirearmsLicenseRequiredToBuy) {
this.FirearmsLicenseRequiredToBuy = FirearmsLicenseRequiredToBuy;
}

/**
* 
* @return
* The Over18DeclarationRequiredToBuy
*/
public Boolean getOver18DeclarationRequiredToBuy() {
return Over18DeclarationRequiredToBuy;
}

/**
* 
* @param Over18DeclarationRequiredToBuy
* The Over18DeclarationRequiredToBuy
*/
public void setOver18DeclarationRequiredToBuy(Boolean Over18DeclarationRequiredToBuy) {
this.Over18DeclarationRequiredToBuy = Over18DeclarationRequiredToBuy;
}

/**
* 
* @return
* The CanOffer
*/
public Boolean getCanOffer() {
return CanOffer;
}

/**
* 
* @param CanOffer
* The CanOffer
*/
public void setCanOffer(Boolean CanOffer) {
this.CanOffer = CanOffer;
}

/**
* 
* @return
* The CanRelist
*/
public Boolean getCanRelist() {
return CanRelist;
}

/**
* 
* @param CanRelist
* The CanRelist
*/
public void setCanRelist(Boolean CanRelist) {
this.CanRelist = CanRelist;
}

/**
* 
* @return
* The WithdrawnBySeller
*/
public Boolean getWithdrawnBySeller() {
return WithdrawnBySeller;
}

/**
* 
* @param WithdrawnBySeller
* The WithdrawnBySeller
*/
public void setWithdrawnBySeller(Boolean WithdrawnBySeller) {
this.WithdrawnBySeller = WithdrawnBySeller;
}

/**
* 
* @return
* The DonationRecipient
*/
public com.apps.vikram.newtrademe.Model.DonationRecipient getDonationRecipient() {
return DonationRecipient;
}

/**
* 
* @param DonationRecipient
* The DonationRecipient
*/
public void setDonationRecipient(com.apps.vikram.newtrademe.Model.DonationRecipient DonationRecipient) {
this.DonationRecipient = DonationRecipient;
}

/**
* 
* @return
* The IsInTradeProtected
*/
public Boolean getIsInTradeProtected() {
return IsInTradeProtected;
}

/**
* 
* @param IsInTradeProtected
* The IsInTradeProtected
*/
public void setIsInTradeProtected(Boolean IsInTradeProtected) {
this.IsInTradeProtected = IsInTradeProtected;
}

/**
* 
* @return
* The IsInCart
*/
public Boolean getIsInCart() {
return IsInCart;
}

/**
* 
* @param IsInCart
* The IsInCart
*/
public void setIsInCart(Boolean IsInCart) {
this.IsInCart = IsInCart;
}

/**
* 
* @return
* The CurrentShippingPromotion
*/
public com.apps.vikram.newtrademe.Model.CurrentShippingPromotion getCurrentShippingPromotion() {
return CurrentShippingPromotion;
}

/**
* 
* @param CurrentShippingPromotion
* The CurrentShippingPromotion
*/
public void setCurrentShippingPromotion(com.apps.vikram.newtrademe.Model.CurrentShippingPromotion CurrentShippingPromotion) {
this.CurrentShippingPromotion = CurrentShippingPromotion;
}

/**
* 
* @return
* The IsLeading
*/
public Boolean getIsLeading() {
return IsLeading;
}

/**
* 
* @param IsLeading
* The IsLeading
*/
public void setIsLeading(Boolean IsLeading) {
this.IsLeading = IsLeading;
}

/**
* 
* @return
* The IsOutbid
*/
public Boolean getIsOutbid() {
return IsOutbid;
}

/**
* 
* @param IsOutbid
* The IsOutbid
*/
public void setIsOutbid(Boolean IsOutbid) {
this.IsOutbid = IsOutbid;
}

/**
* 
* @return
* The CanAddToCart
*/
public Boolean getCanAddToCart() {
return CanAddToCart;
}

/**
* 
* @param CanAddToCart
* The CanAddToCart
*/
public void setCanAddToCart(Boolean CanAddToCart) {
this.CanAddToCart = CanAddToCart;
}

/**
* 
* @return
* The CartItemId
*/
public Integer getCartItemId() {
return CartItemId;
}

/**
* 
* @param CartItemId
* The CartItemId
*/
public void setCartItemId(Integer CartItemId) {
this.CartItemId = CartItemId;
}

/**
* 
* @return
* The MemberProfile
*/
public com.apps.vikram.newtrademe.Model.MemberProfile getMemberProfile() {
return MemberProfile;
}

/**
* 
* @param MemberProfile
* The MemberProfile
*/
public void setMemberProfile(com.apps.vikram.newtrademe.Model.MemberProfile MemberProfile) {
this.MemberProfile = MemberProfile;
}

/**
* 
* @return
* The ViewingTrackerSupported
*/
public Boolean getViewingTrackerSupported() {
return ViewingTrackerSupported;
}

/**
* 
* @param ViewingTrackerSupported
* The ViewingTrackerSupported
*/
public void setViewingTrackerSupported(Boolean ViewingTrackerSupported) {
this.ViewingTrackerSupported = ViewingTrackerSupported;
}

/**
* 
* @return
* The CurrentAutoBid
*/
public Double getCurrentAutoBid() {
return CurrentAutoBid;
}

/**
* 
* @param CurrentAutoBid
* The CurrentAutoBid
*/
public void setCurrentAutoBid(Double CurrentAutoBid) {
this.CurrentAutoBid = CurrentAutoBid;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}