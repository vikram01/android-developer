package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class QuestionsList {

private Integer ListingId;
private Integer ListingQuestionId;
private String Comment;
private String CommentDate;
private String Answer;
private String AnswerDate;
private Boolean IsSellerComment;
private com.apps.vikram.newtrademe.Model.AskingMember AskingMember;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The ListingId
*/
public Integer getListingId() {
return ListingId;
}

/**
* 
* @param ListingId
* The ListingId
*/
public void setListingId(Integer ListingId) {
this.ListingId = ListingId;
}

/**
* 
* @return
* The ListingQuestionId
*/
public Integer getListingQuestionId() {
return ListingQuestionId;
}

/**
* 
* @param ListingQuestionId
* The ListingQuestionId
*/
public void setListingQuestionId(Integer ListingQuestionId) {
this.ListingQuestionId = ListingQuestionId;
}

/**
* 
* @return
* The Comment
*/
public String getComment() {
return Comment;
}

/**
* 
* @param Comment
* The Comment
*/
public void setComment(String Comment) {
this.Comment = Comment;
}

/**
* 
* @return
* The CommentDate
*/
public String getCommentDate() {
return CommentDate;
}

/**
* 
* @param CommentDate
* The CommentDate
*/
public void setCommentDate(String CommentDate) {
this.CommentDate = CommentDate;
}

/**
* 
* @return
* The Answer
*/
public String getAnswer() {
return Answer;
}

/**
* 
* @param Answer
* The Answer
*/
public void setAnswer(String Answer) {
this.Answer = Answer;
}

/**
* 
* @return
* The AnswerDate
*/
public String getAnswerDate() {
return AnswerDate;
}

/**
* 
* @param AnswerDate
* The AnswerDate
*/
public void setAnswerDate(String AnswerDate) {
this.AnswerDate = AnswerDate;
}

/**
* 
* @return
* The IsSellerComment
*/
public Boolean getIsSellerComment() {
return IsSellerComment;
}

/**
* 
* @param IsSellerComment
* The IsSellerComment
*/
public void setIsSellerComment(Boolean IsSellerComment) {
this.IsSellerComment = IsSellerComment;
}

/**
* 
* @return
* The AskingMember
*/
public com.apps.vikram.newtrademe.Model.AskingMember getAskingMember() {
return AskingMember;
}

/**
* 
* @param AskingMember
* The AskingMember
*/
public void setAskingMember(com.apps.vikram.newtrademe.Model.AskingMember AskingMember) {
this.AskingMember = AskingMember;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}