package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class Recipient {

private Member_ Member;
private Integer Decision;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The Member
*/
public Member_ getMember() {
return Member;
}

/**
* 
* @param Member
* The Member
*/
public void setMember(Member_ Member) {
this.Member = Member;
}

/**
* 
* @return
* The Decision
*/
public Integer getDecision() {
return Decision;
}

/**
* 
* @param Decision
* The Decision
*/
public void setDecision(Integer Decision) {
this.Decision = Decision;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
