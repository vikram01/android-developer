package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class ShippingOption {

private Integer Type;
private Double Price;
private String Method;
private Integer ShippingId;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The Type
*/
public Integer getType() {
return Type;
}

/**
* 
* @param Type
* The Type
*/
public void setType(Integer Type) {
this.Type = Type;
}

/**
* 
* @return
* The Price
*/
public Double getPrice() {
return Price;
}

/**
* 
* @param Price
* The Price
*/
public void setPrice(Double Price) {
this.Price = Price;
}

/**
* 
* @return
* The Method
*/
public String getMethod() {
return Method;
}

/**
* 
* @param Method
* The Method
*/
public void setMethod(String Method) {
this.Method = Method;
}

/**
* 
* @return
* The ShippingId
*/
public Integer getShippingId() {
return ShippingId;
}

/**
* 
* @param ShippingId
* The ShippingId
*/
public void setShippingId(Integer ShippingId) {
this.ShippingId = ShippingId;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
