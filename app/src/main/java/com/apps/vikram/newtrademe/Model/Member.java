package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class Member {

private Integer MemberId;
private String Nickname;
private String DateAddressVerified;
private String DateJoined;
private String Email;
private Integer UniqueNegative;
private Integer UniquePositive;
private Integer FeedbackCount;
private Boolean IsAddressVerified;
private String Suburb;
private String Region;
private Boolean IsDealer;
private Boolean IsAuthenticated;
private Boolean IsInTrade;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The MemberId
*/
public Integer getMemberId() {
return MemberId;
}

/**
* 
* @param MemberId
* The MemberId
*/
public void setMemberId(Integer MemberId) {
this.MemberId = MemberId;
}

/**
* 
* @return
* The Nickname
*/
public String getNickname() {
return Nickname;
}

/**
* 
* @param Nickname
* The Nickname
*/
public void setNickname(String Nickname) {
this.Nickname = Nickname;
}

/**
* 
* @return
* The DateAddressVerified
*/
public String getDateAddressVerified() {
return DateAddressVerified;
}

/**
* 
* @param DateAddressVerified
* The DateAddressVerified
*/
public void setDateAddressVerified(String DateAddressVerified) {
this.DateAddressVerified = DateAddressVerified;
}

/**
* 
* @return
* The DateJoined
*/
public String getDateJoined() {
return DateJoined;
}

/**
* 
* @param DateJoined
* The DateJoined
*/
public void setDateJoined(String DateJoined) {
this.DateJoined = DateJoined;
}

/**
* 
* @return
* The Email
*/
public String getEmail() {
return Email;
}

/**
* 
* @param Email
* The Email
*/
public void setEmail(String Email) {
this.Email = Email;
}

/**
* 
* @return
* The UniqueNegative
*/
public Integer getUniqueNegative() {
return UniqueNegative;
}

/**
* 
* @param UniqueNegative
* The UniqueNegative
*/
public void setUniqueNegative(Integer UniqueNegative) {
this.UniqueNegative = UniqueNegative;
}

/**
* 
* @return
* The UniquePositive
*/
public Integer getUniquePositive() {
return UniquePositive;
}

/**
* 
* @param UniquePositive
* The UniquePositive
*/
public void setUniquePositive(Integer UniquePositive) {
this.UniquePositive = UniquePositive;
}

/**
* 
* @return
* The FeedbackCount
*/
public Integer getFeedbackCount() {
return FeedbackCount;
}

/**
* 
* @param FeedbackCount
* The FeedbackCount
*/
public void setFeedbackCount(Integer FeedbackCount) {
this.FeedbackCount = FeedbackCount;
}

/**
* 
* @return
* The IsAddressVerified
*/
public Boolean getIsAddressVerified() {
return IsAddressVerified;
}

/**
* 
* @param IsAddressVerified
* The IsAddressVerified
*/
public void setIsAddressVerified(Boolean IsAddressVerified) {
this.IsAddressVerified = IsAddressVerified;
}

/**
* 
* @return
* The Suburb
*/
public String getSuburb() {
return Suburb;
}

/**
* 
* @param Suburb
* The Suburb
*/
public void setSuburb(String Suburb) {
this.Suburb = Suburb;
}

/**
* 
* @return
* The Region
*/
public String getRegion() {
return Region;
}

/**
* 
* @param Region
* The Region
*/
public void setRegion(String Region) {
this.Region = Region;
}

/**
* 
* @return
* The IsDealer
*/
public Boolean getIsDealer() {
return IsDealer;
}

/**
* 
* @param IsDealer
* The IsDealer
*/
public void setIsDealer(Boolean IsDealer) {
this.IsDealer = IsDealer;
}

/**
* 
* @return
* The IsAuthenticated
*/
public Boolean getIsAuthenticated() {
return IsAuthenticated;
}

/**
* 
* @param IsAuthenticated
* The IsAuthenticated
*/
public void setIsAuthenticated(Boolean IsAuthenticated) {
this.IsAuthenticated = IsAuthenticated;
}

/**
* 
* @return
* The IsInTrade
*/
public Boolean getIsInTrade() {
return IsInTrade;
}

/**
* 
* @param IsInTrade
* The IsInTrade
*/
public void setIsInTrade(Boolean IsInTrade) {
this.IsInTrade = IsInTrade;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}