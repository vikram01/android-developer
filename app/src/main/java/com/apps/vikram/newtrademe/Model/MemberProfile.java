package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class MemberProfile {

private String Occupation;
private String Biography;
private String Quote;
private String Photo;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The Occupation
*/
public String getOccupation() {
return Occupation;
}

/**
* 
* @param Occupation
* The Occupation
*/
public void setOccupation(String Occupation) {
this.Occupation = Occupation;
}

/**
* 
* @return
* The Biography
*/
public String getBiography() {
return Biography;
}

/**
* 
* @param Biography
* The Biography
*/
public void setBiography(String Biography) {
this.Biography = Biography;
}

/**
* 
* @return
* The Quote
*/
public String getQuote() {
return Quote;
}

/**
* 
* @param Quote
* The Quote
*/
public void setQuote(String Quote) {
this.Quote = Quote;
}

/**
* 
* @return
* The Photo
*/
public String getPhoto() {
return Photo;
}

/**
* 
* @param Photo
* The Photo
*/
public void setPhoto(String Photo) {
this.Photo = Photo;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
