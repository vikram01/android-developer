package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class Sale {

private Integer PurchaseId;
private String ReferenceNumber;
private String SoldDate;
private Integer Method;
private String SelectedShipping;
private Double ShippingPrice;
private Integer ShippingType;
private com.apps.vikram.newtrademe.Model.Buyer Buyer;
private Integer QuantitySold;
private Double Price;
private Double SubtotalPrice;
private Double TotalShippingPrice;
private Double TotalSalePrice;
private Boolean HasSellerPlacedFeedback;
private Boolean HasBuyerPlacedFeedback;
private Boolean DeliveryDetailsSent;
private com.apps.vikram.newtrademe.Model.DeliveryAddress DeliveryAddress;
private String MessageFromBuyer;
private String PaymentInstructions;
private Boolean HasPaidByCreditCard;
private Boolean IsPaymentPending;
private String CreditCardPaymentDate;
private String CreditCardType;
private String CreditCardLastFourDigits;
private Integer OrderId;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The PurchaseId
*/
public Integer getPurchaseId() {
return PurchaseId;
}

/**
* 
* @param PurchaseId
* The PurchaseId
*/
public void setPurchaseId(Integer PurchaseId) {
this.PurchaseId = PurchaseId;
}

/**
* 
* @return
* The ReferenceNumber
*/
public String getReferenceNumber() {
return ReferenceNumber;
}

/**
* 
* @param ReferenceNumber
* The ReferenceNumber
*/
public void setReferenceNumber(String ReferenceNumber) {
this.ReferenceNumber = ReferenceNumber;
}

/**
* 
* @return
* The SoldDate
*/
public String getSoldDate() {
return SoldDate;
}

/**
* 
* @param SoldDate
* The SoldDate
*/
public void setSoldDate(String SoldDate) {
this.SoldDate = SoldDate;
}

/**
* 
* @return
* The Method
*/
public Integer getMethod() {
return Method;
}

/**
* 
* @param Method
* The Method
*/
public void setMethod(Integer Method) {
this.Method = Method;
}

/**
* 
* @return
* The SelectedShipping
*/
public String getSelectedShipping() {
return SelectedShipping;
}

/**
* 
* @param SelectedShipping
* The SelectedShipping
*/
public void setSelectedShipping(String SelectedShipping) {
this.SelectedShipping = SelectedShipping;
}

/**
* 
* @return
* The ShippingPrice
*/
public Double getShippingPrice() {
return ShippingPrice;
}

/**
* 
* @param ShippingPrice
* The ShippingPrice
*/
public void setShippingPrice(Double ShippingPrice) {
this.ShippingPrice = ShippingPrice;
}

/**
* 
* @return
* The ShippingType
*/
public Integer getShippingType() {
return ShippingType;
}

/**
* 
* @param ShippingType
* The ShippingType
*/
public void setShippingType(Integer ShippingType) {
this.ShippingType = ShippingType;
}

/**
* 
* @return
* The Buyer
*/
public com.apps.vikram.newtrademe.Model.Buyer getBuyer() {
return Buyer;
}

/**
* 
* @param Buyer
* The Buyer
*/
public void setBuyer(com.apps.vikram.newtrademe.Model.Buyer Buyer) {
this.Buyer = Buyer;
}

/**
* 
* @return
* The QuantitySold
*/
public Integer getQuantitySold() {
return QuantitySold;
}

/**
* 
* @param QuantitySold
* The QuantitySold
*/
public void setQuantitySold(Integer QuantitySold) {
this.QuantitySold = QuantitySold;
}

/**
* 
* @return
* The Price
*/
public Double getPrice() {
return Price;
}

/**
* 
* @param Price
* The Price
*/
public void setPrice(Double Price) {
this.Price = Price;
}

/**
* 
* @return
* The SubtotalPrice
*/
public Double getSubtotalPrice() {
return SubtotalPrice;
}

/**
* 
* @param SubtotalPrice
* The SubtotalPrice
*/
public void setSubtotalPrice(Double SubtotalPrice) {
this.SubtotalPrice = SubtotalPrice;
}

/**
* 
* @return
* The TotalShippingPrice
*/
public Double getTotalShippingPrice() {
return TotalShippingPrice;
}

/**
* 
* @param TotalShippingPrice
* The TotalShippingPrice
*/
public void setTotalShippingPrice(Double TotalShippingPrice) {
this.TotalShippingPrice = TotalShippingPrice;
}

/**
* 
* @return
* The TotalSalePrice
*/
public Double getTotalSalePrice() {
return TotalSalePrice;
}

/**
* 
* @param TotalSalePrice
* The TotalSalePrice
*/
public void setTotalSalePrice(Double TotalSalePrice) {
this.TotalSalePrice = TotalSalePrice;
}

/**
* 
* @return
* The HasSellerPlacedFeedback
*/
public Boolean getHasSellerPlacedFeedback() {
return HasSellerPlacedFeedback;
}

/**
* 
* @param HasSellerPlacedFeedback
* The HasSellerPlacedFeedback
*/
public void setHasSellerPlacedFeedback(Boolean HasSellerPlacedFeedback) {
this.HasSellerPlacedFeedback = HasSellerPlacedFeedback;
}

/**
* 
* @return
* The HasBuyerPlacedFeedback
*/
public Boolean getHasBuyerPlacedFeedback() {
return HasBuyerPlacedFeedback;
}

/**
* 
* @param HasBuyerPlacedFeedback
* The HasBuyerPlacedFeedback
*/
public void setHasBuyerPlacedFeedback(Boolean HasBuyerPlacedFeedback) {
this.HasBuyerPlacedFeedback = HasBuyerPlacedFeedback;
}

/**
* 
* @return
* The DeliveryDetailsSent
*/
public Boolean getDeliveryDetailsSent() {
return DeliveryDetailsSent;
}

/**
* 
* @param DeliveryDetailsSent
* The DeliveryDetailsSent
*/
public void setDeliveryDetailsSent(Boolean DeliveryDetailsSent) {
this.DeliveryDetailsSent = DeliveryDetailsSent;
}

/**
* 
* @return
* The DeliveryAddress
*/
public com.apps.vikram.newtrademe.Model.DeliveryAddress getDeliveryAddress() {
return DeliveryAddress;
}

/**
* 
* @param DeliveryAddress
* The DeliveryAddress
*/
public void setDeliveryAddress(com.apps.vikram.newtrademe.Model.DeliveryAddress DeliveryAddress) {
this.DeliveryAddress = DeliveryAddress;
}

/**
* 
* @return
* The MessageFromBuyer
*/
public String getMessageFromBuyer() {
return MessageFromBuyer;
}

/**
* 
* @param MessageFromBuyer
* The MessageFromBuyer
*/
public void setMessageFromBuyer(String MessageFromBuyer) {
this.MessageFromBuyer = MessageFromBuyer;
}

/**
* 
* @return
* The PaymentInstructions
*/
public String getPaymentInstructions() {
return PaymentInstructions;
}

/**
* 
* @param PaymentInstructions
* The PaymentInstructions
*/
public void setPaymentInstructions(String PaymentInstructions) {
this.PaymentInstructions = PaymentInstructions;
}

/**
* 
* @return
* The HasPaidByCreditCard
*/
public Boolean getHasPaidByCreditCard() {
return HasPaidByCreditCard;
}

/**
* 
* @param HasPaidByCreditCard
* The HasPaidByCreditCard
*/
public void setHasPaidByCreditCard(Boolean HasPaidByCreditCard) {
this.HasPaidByCreditCard = HasPaidByCreditCard;
}

/**
* 
* @return
* The IsPaymentPending
*/
public Boolean getIsPaymentPending() {
return IsPaymentPending;
}

/**
* 
* @param IsPaymentPending
* The IsPaymentPending
*/
public void setIsPaymentPending(Boolean IsPaymentPending) {
this.IsPaymentPending = IsPaymentPending;
}

/**
* 
* @return
* The CreditCardPaymentDate
*/
public String getCreditCardPaymentDate() {
return CreditCardPaymentDate;
}

/**
* 
* @param CreditCardPaymentDate
* The CreditCardPaymentDate
*/
public void setCreditCardPaymentDate(String CreditCardPaymentDate) {
this.CreditCardPaymentDate = CreditCardPaymentDate;
}

/**
* 
* @return
* The CreditCardType
*/
public String getCreditCardType() {
return CreditCardType;
}

/**
* 
* @param CreditCardType
* The CreditCardType
*/
public void setCreditCardType(String CreditCardType) {
this.CreditCardType = CreditCardType;
}

/**
* 
* @return
* The CreditCardLastFourDigits
*/
public String getCreditCardLastFourDigits() {
return CreditCardLastFourDigits;
}

/**
* 
* @param CreditCardLastFourDigits
* The CreditCardLastFourDigits
*/
public void setCreditCardLastFourDigits(String CreditCardLastFourDigits) {
this.CreditCardLastFourDigits = CreditCardLastFourDigits;
}

/**
* 
* @return
* The OrderId
*/
public Integer getOrderId() {
return OrderId;
}

/**
* 
* @param OrderId
* The OrderId
*/
public void setOrderId(Integer OrderId) {
this.OrderId = OrderId;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
