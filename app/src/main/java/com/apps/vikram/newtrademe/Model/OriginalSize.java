package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class OriginalSize {

private Integer Width;
private Integer Height;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The Width
*/
public Integer getWidth() {
return Width;
}

/**
* 
* @param Width
* The Width
*/
public void setWidth(Integer Width) {
this.Width = Width;
}

/**
* 
* @return
* The Height
*/
public Integer getHeight() {
return Height;
}

/**
* 
* @param Height
* The Height
*/
public void setHeight(Integer Height) {
this.Height = Height;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}