package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class DeliveryAddress {

private String Name;
private String Address1;
private String Address2;
private String Suburb;
private String City;
private String Postcode;
private String Country;
private String PhoneNumber;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The Name
*/
public String getName() {
return Name;
}

/**
* 
* @param Name
* The Name
*/
public void setName(String Name) {
this.Name = Name;
}

/**
* 
* @return
* The Address1
*/
public String getAddress1() {
return Address1;
}

/**
* 
* @param Address1
* The Address1
*/
public void setAddress1(String Address1) {
this.Address1 = Address1;
}

/**
* 
* @return
* The Address2
*/
public String getAddress2() {
return Address2;
}

/**
* 
* @param Address2
* The Address2
*/
public void setAddress2(String Address2) {
this.Address2 = Address2;
}

/**
* 
* @return
* The Suburb
*/
public String getSuburb() {
return Suburb;
}

/**
* 
* @param Suburb
* The Suburb
*/
public void setSuburb(String Suburb) {
this.Suburb = Suburb;
}

/**
* 
* @return
* The City
*/
public String getCity() {
return City;
}

/**
* 
* @param City
* The City
*/
public void setCity(String City) {
this.City = City;
}

/**
* 
* @return
* The Postcode
*/
public String getPostcode() {
return Postcode;
}

/**
* 
* @param Postcode
* The Postcode
*/
public void setPostcode(String Postcode) {
this.Postcode = Postcode;
}

/**
* 
* @return
* The Country
*/
public String getCountry() {
return Country;
}

/**
* 
* @param Country
* The Country
*/
public void setCountry(String Country) {
this.Country = Country;
}

/**
* 
* @return
* The PhoneNumber
*/
public String getPhoneNumber() {
return PhoneNumber;
}

/**
* 
* @param PhoneNumber
* The PhoneNumber
*/
public void setPhoneNumber(String PhoneNumber) {
this.PhoneNumber = PhoneNumber;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}