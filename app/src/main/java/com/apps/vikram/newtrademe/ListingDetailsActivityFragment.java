package com.apps.vikram.newtrademe;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.vikram.newtrademe.ApiInterface.RestService;
import com.apps.vikram.newtrademe.Model.ListingDetail;
import com.squareup.picasso.Picasso;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A placeholder fragment containing a simple view.
 */
public class ListingDetailsActivityFragment extends Fragment {

    private RestAdapter restAdapter = null;
    private Integer mListingId = null;
    private static final String Log_Tag ="ListingDetailsFragment";
    private TextView tvListingTitle;
    private TextView tvListingId;
    private ImageView imgViewListing;


    public ListingDetailsActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listing_details, container, false);
        tvListingId = (TextView) view.findViewById(R.id.tvListingDetailId);
        tvListingTitle = (TextView) view.findViewById(R.id.tvListingDetailTitle);
        imgViewListing = (ImageView) view.findViewById(R.id.imgViewListingDetail);


//        RequestInterceptor requestInterceptor = new RequestInterceptor() {
//            @Override
//            public void intercept(RequestFacade request) {
//
//                String oAuthStr= "OAuth oauth_consumer_key=\"" + getString(R.string.consumer_key)+
//                        "\", oauth_signature_method=\"PLAINTEXT\", oauth_signature=\""+getString(R.string.consumer_secret) +"&\"";
//                Log.d(Log_Tag, oAuthStr);
//                request.addHeader("Authorization", oAuthStr);
//                Log.d(Log_Tag, request.toString());
//
//            }
//        };
//        restAdapter = new RestAdapter.Builder()
//                .setEndpoint(getString(R.string.rest_base))
//                .setRequestInterceptor(requestInterceptor)
//                .build();
//
        mListingId = getActivity().getIntent().getExtras().getInt("LISTING_ID");

//        TrademeApiInterface apiService = restAdapter.create(TrademeApiInterface.class);

        Log.d(Log_Tag, "-->ListingId-" + mListingId);
  //      apiService.listingDetail(mListingId, new Callback<ListingDetail>() {
        new RestService(getActivity()).callService().listingDetail(mListingId, new Callback<ListingDetail>() {
            @Override
            public void success(ListingDetail listingDetail, Response response) {

                Log.d(Log_Tag, "Success-" + mListingId);
                populateViews(listingDetail);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(Log_Tag, "Failure-" + mListingId);

                error.printStackTrace();

            }
        });


        return view;
    }

    void populateViews(ListingDetail listingDetail)
    {
        try
        {
            tvListingTitle.setText(listingDetail.getTitle());
            tvListingId.setText(listingDetail.getListingId().toString());
            Log.d(Log_Tag, "-->Thumbnail-" + listingDetail.getPhotos().get(0).getValue().getThumbnail());

            if(listingDetail.getPhotos().size()>0 && !listingDetail.getPhotos().get(0).getValue().getGallery().isEmpty()) {
                Picasso.with(getActivity()).
                        load(listingDetail.
                                getPhotos().
                                get(0).
                                getValue().
                                getGallery()).into(imgViewListing);
            }
            TextView tvLocation = (TextView) getActivity().findViewById(R.id.tvLocation);
            tvLocation.setText(listingDetail.getRegion());
            TextView tvBody = (TextView) getActivity().findViewById(R.id.tvBody);
            tvBody.setText(listingDetail.getBody());
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
