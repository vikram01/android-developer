package com.apps.vikram.newtrademe;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.apps.vikram.newtrademe.ApiInterface.RestService;
import com.apps.vikram.newtrademe.Logic.ListingAdapter;
import com.apps.vikram.newtrademe.Model.List;
import com.apps.vikram.newtrademe.Model.SearchResult;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ShowListingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShowListingsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_CATEGORY = "CATEGORY";
    private static final String ARG_PARAM2 = "param2";
    private static final String Log_Tag = "ShowListingsFragment";

    // TODO: Rename and change types of parameters
    private String mCategory;
    private String mParam2;

    private ShowListingFragmentInterface mListener;

    private ArrayList<List> mListingList = null;
    private RestAdapter restAdapter = null;
    private ListingAdapter listingAdapter = null;

    private AdapterView.OnItemClickListener listingItemOnClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
           List selectedItem = (List) parent.getItemAtPosition(position);
           Log.d(Log_Tag,"--inside in click"+selectedItem) ;

           if(selectedItem != null)
           {
              mListener.showListingDetails(selectedItem.getListingId());
           }

        }
    } ;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param category Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ShowListingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShowListingsFragment newInstance(String category, String param2) {
        ShowListingsFragment fragment = new ShowListingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CATEGORY, category);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ShowListingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCategory = getArguments().getString(ARG_CATEGORY);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_show_listings, container, false);// pass true here for dynamic inflation
        ListView listView = (ListView) view.findViewById(R.id.listingsListView);

        mListingList = new ArrayList<>();
        listingAdapter = new ListingAdapter(getActivity(),mListingList);
        listView.setAdapter(listingAdapter);
        listView.setOnItemClickListener(listingItemOnClick);
        listView.setEmptyView(view.findViewById(R.id.empty));
        listView.getEmptyView().setVisibility(View.INVISIBLE);


//        RequestInterceptor requestInterceptor = new RequestInterceptor() {
//            @Override
//            public void intercept(RequestFacade request) {
//
//                String oAuthStr= "OAuth oauth_consumer_key=\"" + getString(R.string.consumer_key)+
//                "\", oauth_signature_method=\"PLAINTEXT\", oauth_signature=\""+getString(R.string.consumer_secret) +"&\"";
//                Log.d(Log_Tag,oAuthStr);
//                request.addHeader("Authorization", oAuthStr);
//                Log.d(Log_Tag, request.toString());
//
//            }
//        };
//        restAdapter = new RestAdapter.Builder()
//                .setEndpoint(getString(R.string.rest_base))
//                .setRequestInterceptor(requestInterceptor)
//                .build();

        Log.d(Log_Tag,"mCategory-------------"+mCategory );

        if(mCategory == null)
        {
            Fragment fragment = getFragmentManager().findFragmentById(R.id.fragmentCategory);
            Log.d(Log_Tag,"mCategory-------------"+mCategory );
            if(fragment== null) {

             //   mCategory = mListener.getCategory(); too early;
                //ViewPager
                //mCategory = getActivity().getIntent().getExtras().getString("CATEGORY");
            }
            else
            {
                mCategory="";
            }
        }
        Log.d(Log_Tag,"mCategory-------------"+mCategory );
        if(mCategory != null)
            showListings(mCategory);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ShowListingFragmentInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ShowListingFragmentInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface ShowListingFragmentInterface {
        // TODO: Update argument type and name
        String getCategory();
        void showListingDetails(Integer listingId);


    }

    public void showListings(String categoryName)
    {
        try {
            Log.d(Log_Tag, "-->CategoryName-" + categoryName);

            new RestService(getActivity()).callService().listingSearch(categoryName, "20", "List", new Callback<SearchResult>() {
                    @Override
                    public void success(SearchResult result, Response response) {
                        Log.d(Log_Tag, "-->Success<listingSearch------------------" + result.getTotalCount());
                        if (result.getList() != null) {
                            mListingList.clear();
                            mListingList.addAll(result.getList());
                            listingAdapter.notifyDataSetChanged();
                        }
                    }
                    @Override
                    public void failure(RetrofitError error) {
                        Log.d(Log_Tag, "-->Failed<------------------");
                        error.printStackTrace();
                    }
                });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

}
