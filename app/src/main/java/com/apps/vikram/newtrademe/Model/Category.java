package com.apps.vikram.newtrademe.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vikram on 5/19/15.
 */
public class Category {

    private String Name;
    private String Number;
    private String Path;
    private List<Category> Subcategories = new ArrayList<Category>();
    private Integer Count;
    private Boolean IsRestricted;
    private Boolean HasLegalNotice;
    private Boolean HasClassifieds;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The Name
     */
    public String getName() {
        return Name;
    }

    /**
     *
     * @param Name
     * The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     *
     * @return
     * The Number
     */
    public String getNumber() {
        return Number;
    }

    /**
     *
     * @param Number
     * The Number
     */
    public void setNumber(String Number) {
        this.Number = Number;
    }

    /**
     *
     * @return
     * The Path
     */
    public String getPath() {
        return Path;
    }

    /**
     *
     * @param Path
     * The Path
     */
    public void setPath(String Path) {
        this.Path = Path;
    }


    public Integer getCount() {
        return Count;
    }

    /**
     *
     * @param Count
     * The Count
     */
    public void setCount(Integer Count) {
        this.Count = Count;
    }

    /**
     *
     * @return
     * The IsRestricted
     */
    public Boolean getIsRestricted() {
        return IsRestricted;
    }

    /**
     *
     * @param IsRestricted
     * The IsRestricted
     */
    public void setIsRestricted(Boolean IsRestricted) {
        this.IsRestricted = IsRestricted;
    }

    /**
     *
     * @return
     * The HasLegalNotice
     */
    public Boolean getHasLegalNotice() {
        return HasLegalNotice;
    }

    /**
     *
     * @param HasLegalNotice
     * The HasLegalNotice
     */
    public void setHasLegalNotice(Boolean HasLegalNotice) {
        this.HasLegalNotice = HasLegalNotice;
    }

    /**
     *
     * @return
     * The HasClassifieds
     */
    public Boolean getHasClassifieds() {
        return HasClassifieds;
    }

    /**
     *
     * @param HasClassifieds
     * The HasClassifieds
     */
    public void setHasClassifieds(Boolean HasClassifieds) {
        this.HasClassifieds = HasClassifieds;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public List<Category> getSubcategories() {
        return Subcategories;
    }

    public void setSubcategories(List<Category> subcategories) {
        Subcategories = subcategories;
    }
}
