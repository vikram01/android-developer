package com.apps.vikram.newtrademe.ApiInterface;

import android.content.Context;

import com.apps.vikram.newtrademe.R;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by Vikram on 5/22/15.
 * Just encapsulation Retrofit and headers
 */
public class RestService {
    private Context mContext;
    private String oAuth;
    private RequestInterceptor requestInterceptor;
    private RestAdapter restAdapter;
    public RestService(Context context)
    {
        mContext = context;
//        oAuth="OAuth oauth_consumer_key=\""C41440C17CF277F5A87BA7F93873FBF7\", oauth_signature_method=\"PLAINTEXT\", oauth_signature=\""+
        oAuth="OAuth oauth_consumer_key=\"C41440C17CF277F5A87BA7F93873FBF7\", oauth_signature_method=\"PLAINTEXT\", oauth_signature=\"154A9709D1542907BB7E30D6D2D9A741&\"";
    }
    public TrademeApiInterface callService()
    {
        requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Authorization", oAuth);
            }
        };

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(mContext.getString(R.string.rest_base))
                .setRequestInterceptor(requestInterceptor)
                .build();
        return  restAdapter.create(TrademeApiInterface.class);
    }
}
