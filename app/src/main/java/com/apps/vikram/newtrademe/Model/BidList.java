package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;


// Originally List
public class BidList {

private String Account;
private Double BidAmount;
private Boolean IsByMobile;
private Boolean IsByProxy;
private String BidDate;
private Boolean IsBuyNow;
private com.apps.vikram.newtrademe.Model.Bidder Bidder;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The Account
*/
public String getAccount() {
return Account;
}

/**
* 
* @param Account
* The Account
*/
public void setAccount(String Account) {
this.Account = Account;
}

/**
* 
* @return
* The BidAmount
*/
public Double getBidAmount() {
return BidAmount;
}

/**
* 
* @param BidAmount
* The BidAmount
*/
public void setBidAmount(Double BidAmount) {
this.BidAmount = BidAmount;
}

/**
* 
* @return
* The IsByMobile
*/
public Boolean getIsByMobile() {
return IsByMobile;
}

/**
* 
* @param IsByMobile
* The IsByMobile
*/
public void setIsByMobile(Boolean IsByMobile) {
this.IsByMobile = IsByMobile;
}

/**
* 
* @return
* The IsByProxy
*/
public Boolean getIsByProxy() {
return IsByProxy;
}

/**
* 
* @param IsByProxy
* The IsByProxy
*/
public void setIsByProxy(Boolean IsByProxy) {
this.IsByProxy = IsByProxy;
}

/**
* 
* @return
* The BidDate
*/
public String getBidDate() {
return BidDate;
}

/**
* 
* @param BidDate
* The BidDate
*/
public void setBidDate(String BidDate) {
this.BidDate = BidDate;
}

/**
* 
* @return
* The IsBuyNow
*/
public Boolean getIsBuyNow() {
return IsBuyNow;
}

/**
* 
* @param IsBuyNow
* The IsBuyNow
*/
public void setIsBuyNow(Boolean IsBuyNow) {
this.IsBuyNow = IsBuyNow;
}

/**
* 
* @return
* The Bidder
*/
public com.apps.vikram.newtrademe.Model.Bidder getBidder() {
return Bidder;
}

/**
* 
* @param Bidder
* The Bidder
*/
public void setBidder(com.apps.vikram.newtrademe.Model.Bidder Bidder) {
this.Bidder = Bidder;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}