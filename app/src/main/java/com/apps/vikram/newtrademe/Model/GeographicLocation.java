package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vikram on 5/19/15.
 */
public class GeographicLocation {
        private Double Latitude;
        private Double Longitude;
        private Integer Northing;
        private Integer Easting;
        private Integer Accuracy;
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         *
         * @return
         * The Latitude
         */
        public Double getLatitude() {
            return Latitude;
        }

        /**
         *
         * @param Latitude
         * The Latitude
         */
        public void setLatitude(Double Latitude) {
            this.Latitude = Latitude;
        }

        /**
         *
         * @return
         * The Longitude
         */
        public Double getLongitude() {
            return Longitude;
        }

        /**
         *
         * @param Longitude
         * The Longitude
         */
        public void setLongitude(Double Longitude) {
            this.Longitude = Longitude;
        }

        /**
         *
         * @return
         * The Northing
         */
        public Integer getNorthing() {
            return Northing;
        }

        /**
         *
         * @param Northing
         * The Northing
         */
        public void setNorthing(Integer Northing) {
            this.Northing = Northing;
        }

        /**
         *
         * @return
         * The Easting
         */
        public Integer getEasting() {
            return Easting;
        }

        /**
         *
         * @param Easting
         * The Easting
         */
        public void setEasting(Integer Easting) {
            this.Easting = Easting;
        }

        /**
         *
         * @return
         * The Accuracy
         */
        public Integer getAccuracy() {
            return Accuracy;
        }

        /**
         *
         * @param Accuracy
         * The Accuracy
         */
        public void setAccuracy(Integer Accuracy) {
            this.Accuracy = Accuracy;
        }

        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }


}
