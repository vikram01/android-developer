package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vikram on 5/19/15.
 *
 */
public class FoundCategory {

    private Integer Count;
    private String Category;
    private String Name;
    private Boolean IsRestricted;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The Count
     */
    public Integer getCount() {
        return Count;
    }

    /**
     *
     * @param Count
     * The Count
     */
    public void setCount(Integer Count) {
        this.Count = Count;
    }

    /**
     *
     * @return
     * The Category
     */
    public String getCategory() {
        return Category;
    }

    /**
     *
     * @param Category
     * The Category
     */
    public void setCategory(String Category) {
        this.Category = Category;
    }

    /**
     *
     * @return
     * The Name
     */
    public String getName() {
        return Name;
    }

    /**
     *
     * @param Name
     * The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     *
     * @return
     * The IsRestricted
     */
    public Boolean getIsRestricted() {
        return IsRestricted;
    }

    /**
     *
     * @param IsRestricted
     * The IsRestricted
     */
    public void setIsRestricted(Boolean IsRestricted) {
        this.IsRestricted = IsRestricted;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}