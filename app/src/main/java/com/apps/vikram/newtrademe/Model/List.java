package com.apps.vikram.newtrademe.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vikram on 5/19/15.
 */
public class List {

    private Integer ListingId;
    private String Title;
    private String Category;
    private Double StartPrice;
    private Double BuyNowPrice;
    private String StartDate;
    private String EndDate;
    private Boolean IsFeatured;
    private Boolean HasGallery;
    private Boolean IsBold;
    private Boolean IsHighlighted;
    private Boolean HasHomePageFeature;
    private Double MaxBidAmount;
    private String AsAt;
    private String CategoryPath;
    private String PictureHref;
    private Boolean HasPayNow;
    private Boolean IsNew;
    private Integer RegionId;
    private String Region;
    private Integer SuburbId;
    private String Suburb;
    private Integer BidCount;
    private Boolean IsReserveMet;
    private Boolean HasReserve;
    private Boolean HasBuyNow;
    private String NoteDate;
    private Integer ReserveState;
    private Boolean IsClassified;
    private java.util.List<OpenHome> OpenHomes = new ArrayList<OpenHome>();
    private String Subtitle;
    private Boolean IsBuyNowOnly;
    private Integer RemainingGalleryPlusRelists;
    private Boolean IsOnWatchList;
    private GeographicLocation GeographicLocation;
    private String PriceDisplay;
    private Integer TotalReviewCount;
    private Integer PositiveReviewCount;
    private Boolean HasFreeShipping;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The ListingId
     */
    public Integer getListingId() {
        return ListingId;
    }

    /**
     *
     * @param ListingId
     * The ListingId
     */
    public void setListingId(Integer ListingId) {
        this.ListingId = ListingId;
    }

    /**
     *
     * @return
     * The Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     *
     * @param Title
     * The Title
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     *
     * @return
     * The Category
     */
    public String getCategory() {
        return Category;
    }

    /**
     *
     * @param Category
     * The Category
     */
    public void setCategory(String Category) {
        this.Category = Category;
    }

    /**
     *
     * @return
     * The StartPrice
     */
    public Double getStartPrice() {
        return StartPrice;
    }

    /**
     *
     * @param StartPrice
     * The StartPrice
     */
    public void setStartPrice(Double StartPrice) {
        this.StartPrice = StartPrice;
    }

    /**
     *
     * @return
     * The BuyNowPrice
     */
    public Double getBuyNowPrice() {
        return BuyNowPrice;
    }

    /**
     *
     * @param BuyNowPrice
     * The BuyNowPrice
     */
    public void setBuyNowPrice(Double BuyNowPrice) {
        this.BuyNowPrice = BuyNowPrice;
    }

    /**
     *
     * @return
     * The StartDate
     */
    public String getStartDate() {
        return StartDate;
    }

    /**
     *
     * @param StartDate
     * The StartDate
     */
    public void setStartDate(String StartDate) {
        this.StartDate = StartDate;
    }

    /**
     *
     * @return
     * The EndDate
     */
    public String getEndDate() {
        return EndDate;
    }

    /**
     *
     * @param EndDate
     * The EndDate
     */
    public void setEndDate(String EndDate) {
        this.EndDate = EndDate;
    }

    /**
     *
     * @return
     * The IsFeatured
     */
    public Boolean getIsFeatured() {
        return IsFeatured;
    }

    /**
     *
     * @param IsFeatured
     * The IsFeatured
     */
    public void setIsFeatured(Boolean IsFeatured) {
        this.IsFeatured = IsFeatured;
    }

    /**
     *
     * @return
     * The HasGallery
     */
    public Boolean getHasGallery() {
        return HasGallery;
    }

    /**
     *
     * @param HasGallery
     * The HasGallery
     */
    public void setHasGallery(Boolean HasGallery) {
        this.HasGallery = HasGallery;
    }

    /**
     *
     * @return
     * The IsBold
     */
    public Boolean getIsBold() {
        return IsBold;
    }

    /**
     *
     * @param IsBold
     * The IsBold
     */
    public void setIsBold(Boolean IsBold) {
        this.IsBold = IsBold;
    }

    /**
     *
     * @return
     * The IsHighlighted
     */
    public Boolean getIsHighlighted() {
        return IsHighlighted;
    }

    /**
     *
     * @param IsHighlighted
     * The IsHighlighted
     */
    public void setIsHighlighted(Boolean IsHighlighted) {
        this.IsHighlighted = IsHighlighted;
    }

    /**
     *
     * @return
     * The HasHomePageFeature
     */
    public Boolean getHasHomePageFeature() {
        return HasHomePageFeature;
    }

    /**
     *
     * @param HasHomePageFeature
     * The HasHomePageFeature
     */
    public void setHasHomePageFeature(Boolean HasHomePageFeature) {
        this.HasHomePageFeature = HasHomePageFeature;
    }

    /**
     *
     * @return
     * The MaxBidAmount
     */
    public Double getMaxBidAmount() {
        return MaxBidAmount;
    }

    /**
     *
     * @param MaxBidAmount
     * The MaxBidAmount
     */
    public void setMaxBidAmount(Double MaxBidAmount) {
        this.MaxBidAmount = MaxBidAmount;
    }

    /**
     *
     * @return
     * The AsAt
     */
    public String getAsAt() {
        return AsAt;
    }

    /**
     *
     * @param AsAt
     * The AsAt
     */
    public void setAsAt(String AsAt) {
        this.AsAt = AsAt;
    }

    /**
     *
     * @return
     * The CategoryPath
     */
    public String getCategoryPath() {
        return CategoryPath;
    }

    /**
     *
     * @param CategoryPath
     * The CategoryPath
     */
    public void setCategoryPath(String CategoryPath) {
        this.CategoryPath = CategoryPath;
    }

    /**
     *
     * @return
     * The PictureHref
     */
    public String getPictureHref() {
        return PictureHref;
    }

    /**
     *
     * @param PictureHref
     * The PictureHref
     */
    public void setPictureHref(String PictureHref) {
        this.PictureHref = PictureHref;
    }

    /**
     *
     * @return
     * The HasPayNow
     */
    public Boolean getHasPayNow() {
        return HasPayNow;
    }

    /**
     *
     * @param HasPayNow
     * The HasPayNow
     */
    public void setHasPayNow(Boolean HasPayNow) {
        this.HasPayNow = HasPayNow;
    }

    /**
     *
     * @return
     * The IsNew
     */
    public Boolean getIsNew() {
        return IsNew;
    }

    /**
     *
     * @param IsNew
     * The IsNew
     */
    public void setIsNew(Boolean IsNew) {
        this.IsNew = IsNew;
    }

    /**
     *
     * @return
     * The RegionId
     */
    public Integer getRegionId() {
        return RegionId;
    }

    /**
     *
     * @param RegionId
     * The RegionId
     */
    public void setRegionId(Integer RegionId) {
        this.RegionId = RegionId;
    }

    /**
     *
     * @return
     * The Region
     */
    public String getRegion() {
        return Region;
    }

    /**
     *
     * @param Region
     * The Region
     */
    public void setRegion(String Region) {
        this.Region = Region;
    }

    /**
     *
     * @return
     * The SuburbId
     */
    public Integer getSuburbId() {
        return SuburbId;
    }

    /**
     *
     * @param SuburbId
     * The SuburbId
     */
    public void setSuburbId(Integer SuburbId) {
        this.SuburbId = SuburbId;
    }

    /**
     *
     * @return
     * The Suburb
     */
    public String getSuburb() {
        return Suburb;
    }

    /**
     *
     * @param Suburb
     * The Suburb
     */
    public void setSuburb(String Suburb) {
        this.Suburb = Suburb;
    }

    /**
     *
     * @return
     * The BidCount
     */
    public Integer getBidCount() {
        return BidCount;
    }

    /**
     *
     * @param BidCount
     * The BidCount
     */
    public void setBidCount(Integer BidCount) {
        this.BidCount = BidCount;
    }

    /**
     *
     * @return
     * The IsReserveMet
     */
    public Boolean getIsReserveMet() {
        return IsReserveMet;
    }

    /**
     *
     * @param IsReserveMet
     * The IsReserveMet
     */
    public void setIsReserveMet(Boolean IsReserveMet) {
        this.IsReserveMet = IsReserveMet;
    }

    /**
     *
     * @return
     * The HasReserve
     */
    public Boolean getHasReserve() {
        return HasReserve;
    }

    /**
     *
     * @param HasReserve
     * The HasReserve
     */
    public void setHasReserve(Boolean HasReserve) {
        this.HasReserve = HasReserve;
    }

    /**
     *
     * @return
     * The HasBuyNow
     */
    public Boolean getHasBuyNow() {
        return HasBuyNow;
    }

    /**
     *
     * @param HasBuyNow
     * The HasBuyNow
     */
    public void setHasBuyNow(Boolean HasBuyNow) {
        this.HasBuyNow = HasBuyNow;
    }

    /**
     *
     * @return
     * The NoteDate
     */
    public String getNoteDate() {
        return NoteDate;
    }

    /**
     *
     * @param NoteDate
     * The NoteDate
     */
    public void setNoteDate(String NoteDate) {
        this.NoteDate = NoteDate;
    }

    /**
     *
     * @return
     * The ReserveState
     */
    public Integer getReserveState() {
        return ReserveState;
    }

    /**
     *
     * @param ReserveState
     * The ReserveState
     */
    public void setReserveState(Integer ReserveState) {
        this.ReserveState = ReserveState;
    }

    /**
     *
     * @return
     * The IsClassified
     */
    public Boolean getIsClassified() {
        return IsClassified;
    }

    /**
     *
     * @param IsClassified
     * The IsClassified
     */
    public void setIsClassified(Boolean IsClassified) {
        this.IsClassified = IsClassified;
    }

    /**
     *
     * @return
     * The OpenHomes
     */
    public java.util.List<OpenHome> getOpenHomes() {
        return OpenHomes;
    }

    /**
     *
     * @param OpenHomes
     * The OpenHomes
     */
    public void setOpenHomes(java.util.List<OpenHome> OpenHomes) {
        this.OpenHomes = OpenHomes;
    }

    /**
     *
     * @return
     * The Subtitle
     */
    public String getSubtitle() {
        return Subtitle;
    }

    /**
     *
     * @param Subtitle
     * The Subtitle
     */
    public void setSubtitle(String Subtitle) {
        this.Subtitle = Subtitle;
    }

    /**
     *
     * @return
     * The IsBuyNowOnly
     */
    public Boolean getIsBuyNowOnly() {
        return IsBuyNowOnly;
    }

    /**
     *
     * @param IsBuyNowOnly
     * The IsBuyNowOnly
     */
    public void setIsBuyNowOnly(Boolean IsBuyNowOnly) {
        this.IsBuyNowOnly = IsBuyNowOnly;
    }

    /**
     *
     * @return
     * The RemainingGalleryPlusRelists
     */
    public Integer getRemainingGalleryPlusRelists() {
        return RemainingGalleryPlusRelists;
    }

    /**
     *
     * @param RemainingGalleryPlusRelists
     * The RemainingGalleryPlusRelists
     */
    public void setRemainingGalleryPlusRelists(Integer RemainingGalleryPlusRelists) {
        this.RemainingGalleryPlusRelists = RemainingGalleryPlusRelists;
    }

    /**
     *
     * @return
     * The IsOnWatchList
     */
    public Boolean getIsOnWatchList() {
        return IsOnWatchList;
    }

    /**
     *
     * @param IsOnWatchList
     * The IsOnWatchList
     */
    public void setIsOnWatchList(Boolean IsOnWatchList) {
        this.IsOnWatchList = IsOnWatchList;
    }

    /**
     *
     * @return
     * The GeographicLocation
     */
    public GeographicLocation getGeographicLocation() {
        return GeographicLocation;
    }

    /**
     *
     * @param GeographicLocation
     * The GeographicLocation
     */
    public void setGeographicLocation(GeographicLocation GeographicLocation) {
        this.GeographicLocation = GeographicLocation;
    }

    /**
     *
     * @return
     * The PriceDisplay
     */
    public String getPriceDisplay() {
        return PriceDisplay;
    }

    /**
     *
     * @param PriceDisplay
     * The PriceDisplay
     */
    public void setPriceDisplay(String PriceDisplay) {
        this.PriceDisplay = PriceDisplay;
    }

    /**
     *
     * @return
     * The TotalReviewCount
     */
    public Integer getTotalReviewCount() {
        return TotalReviewCount;
    }

    /**
     *
     * @param TotalReviewCount
     * The TotalReviewCount
     */
    public void setTotalReviewCount(Integer TotalReviewCount) {
        this.TotalReviewCount = TotalReviewCount;
    }

    /**
     *
     * @return
     * The PositiveReviewCount
     */
    public Integer getPositiveReviewCount() {
        return PositiveReviewCount;
    }

    /**
     *
     * @param PositiveReviewCount
     * The PositiveReviewCount
     */
    public void setPositiveReviewCount(Integer PositiveReviewCount) {
        this.PositiveReviewCount = PositiveReviewCount;
    }

    /**
     *
     * @return
     * The HasFreeShipping
     */
    public Boolean getHasFreeShipping() {
        return HasFreeShipping;
    }

    /**
     *
     * @param HasFreeShipping
     * The HasFreeShipping
     */
    public void setHasFreeShipping(Boolean HasFreeShipping) {
        this.HasFreeShipping = HasFreeShipping;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}