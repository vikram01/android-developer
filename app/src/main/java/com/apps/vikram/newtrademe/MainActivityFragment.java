package com.apps.vikram.newtrademe;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.apps.vikram.newtrademe.ApiInterface.RestService;
import com.apps.vikram.newtrademe.Logic.CategoryAdapter;
import com.apps.vikram.newtrademe.Model.Category;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public static final String Log_Tag = "MainActivityFragment";
    private CategoryAdapter mCategoryAdapter = null;
    private ArrayList<Category> mCategoryList = null;
    private ShowCategoryInterface mListener = null;

    private OnItemClickListener categoryOnItemClickListner = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            String categoryName = "/"+ mCategoryAdapter.getItem(position).getNumber();
            getCategories(categoryName);
        }
    };


    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        Log.d(Log_Tag,"XXXXXXXXXXXXXXXXXXXXX--onCreateView--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mCategoryList = new ArrayList<>();
        mCategoryAdapter = new CategoryAdapter(getActivity(),mCategoryList);

        ListView listView = (ListView)  view.findViewById(R.id.categoryListView);

        listView.setOnItemClickListener(categoryOnItemClickListner);
        listView.setAdapter(mCategoryAdapter);
        getCategories("");
//        RequestInterceptor requestInterceptor = new RequestInterceptor() {
//            @Override
//            public void intercept(RequestFacade request) {
//                request.addHeader("oauth_consumer_key", getString(R.string.consumer_key));
//                request.addHeader("oauth_signature_method", "PLAINTEXT");
//                request.addHeader("oauth_signature",getString(R.string.consumer_secret));
//
//            }
//        };
//
//        restAdapter = new RestAdapter.Builder()
//                .setEndpoint(getString(R.string.rest_base))
//                .setRequestInterceptor(requestInterceptor)
//                .build();

//        TrademeApiInterface apiService = restAdapter.create(TrademeApiInterface.class);
//
//        apiService.getSubCategories("", new Callback<Category>() {
//
//
//            @Override
//            public void success(Category categories, Response response) {
//
//                Log.d(Log_Tag, "In success-----------------------------" + categories.getSubcategories().size());
//                Log.d(Log_Tag, "--" + categories.getSubcategories().get(0).getSubcategories().get(0).getPath());
//                mCategoryList.clear();
//                mCategoryList.addAll(categories.getSubcategories());
//                if (mCategoryAdapter != null)
//                    mCategoryAdapter.notifyDataSetChanged();
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//                error.printStackTrace();
//            }
//        });
        return view;
    }


    private void startListingFragment(String category)
    {
        mListener.showListings(category);
    }

    public  interface ShowCategoryInterface {
        void showListings(String category);

    }

    public  Boolean simulateBackPress()
    {
        if(mCategoryList.get(0)!= null) {


            String currentCategory = mCategoryList.get(0).getNumber();
            String midCategory = "";
            String previousCategory = "";
            Log.d(Log_Tag, "categ-->" + currentCategory);
            int length = currentCategory.length()-1;
            int delimiter= currentCategory.substring(0,length).lastIndexOf("-");
            if(delimiter>1)
            {
                midCategory = currentCategory.substring(0,delimiter+1);
            }

            length = midCategory.length()-1;
            delimiter=0;
            if(length>1)
                delimiter= midCategory.substring(0,length).lastIndexOf("-");

            Log.d(Log_Tag, "midcateg-->" + midCategory);
            if(delimiter>1)
            {
                previousCategory = midCategory.substring(0,delimiter+1);
            }
            else if (length>1){
                getCategories("");
                return true;
            }
            Log.d(Log_Tag, "prevcateg-->" + previousCategory);

            if(previousCategory !=null && !previousCategory.isEmpty()) {
                getCategories("/"+previousCategory);
                return true;
            }
        }
        return  false;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ShowCategoryInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ShowListingFragmentInterface");
        }
    }

    public void onCategoryClick(Category categories)
    {
        Log.d(Log_Tag,"Inside onCategoryClick"+ categories.getNumber());

        try {

            ShowListingsFragment listingsFragment = (ShowListingsFragment) getFragmentManager().findFragmentById(R.id.fragmentListing);

            ViewPager pager = (ViewPager) getActivity().findViewById(R.id.pager);

            if ((categories.getSubcategories().isEmpty() && listingsFragment  == null)
                    || (categories.getSubcategories().isEmpty() && listingsFragment!= null && listingsFragment.isVisible() )
                    || listingsFragment != null
                    ||(pager!= null && pager.getVisibility()== View.VISIBLE)
                    ) //No more sub categories or Landscape view
            {
                //Show Listing Fragment
                startListingFragment(categories.getNumber());

            }

            if (!categories.getSubcategories().isEmpty()) {
                mCategoryList.clear();
                mCategoryList.addAll(categories.getSubcategories());
                if (mCategoryAdapter != null)
                    mCategoryAdapter.notifyDataSetChanged();
            }
            else if(pager != null && pager.getVisibility()==View.VISIBLE)
            {
                pager.setCurrentItem(1);
            }


        }
        catch (Exception ex)
        {
            //This catches crash when screen orientation changed while loading app -- getFragmentManager null
            ex.printStackTrace();
        }
    }
    public void getCategories(String categoryName)
    {

        //TrademeApiInterface apiService = restAdapter.create(TrademeApiInterface.class);

        //apiService.getSubCategories(categoryName, new Callback<Category>() {
        try {
            new RestService(getActivity()).callService().getSubCategories(categoryName, new Callback<Category>() {
                @Override
                public void success(Category categories, Response response) {

                    onCategoryClick(categories);
                }

                @Override
                public void failure(RetrofitError error) {

                    Log.d(Log_Tag, "In Failure-----getCategories");
                    error.printStackTrace();
                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

}
