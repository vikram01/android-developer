package com.apps.vikram.newtrademe.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 * Created by Vikram on 5/19/15.
 */
public class Parameter {

    private String DisplayName;
    private String Name;
    private String LowerBoundName;
    private String UpperBoundName;
    private Integer Type;
    private Boolean AllowsMultipleValues;
    private List<Option> Options = new ArrayList<Option>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The DisplayName
     */
    public String getDisplayName() {
        return DisplayName;
    }

    /**
     *
     * @param DisplayName
     * The DisplayName
     */
    public void setDisplayName(String DisplayName) {
        this.DisplayName = DisplayName;
    }

    /**
     *
     * @return
     * The Name
     */
    public String getName() {
        return Name;
    }

    /**
     *
     * @param Name
     * The Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     *
     * @return
     * The LowerBoundName
     */
    public String getLowerBoundName() {
        return LowerBoundName;
    }

    /**
     *
     * @param LowerBoundName
     * The LowerBoundName
     */
    public void setLowerBoundName(String LowerBoundName) {
        this.LowerBoundName = LowerBoundName;
    }

    /**
     *
     * @return
     * The UpperBoundName
     */
    public String getUpperBoundName() {
        return UpperBoundName;
    }

    /**
     *
     * @param UpperBoundName
     * The UpperBoundName
     */
    public void setUpperBoundName(String UpperBoundName) {
        this.UpperBoundName = UpperBoundName;
    }

    /**
     *
     * @return
     * The Type
     */
    public Integer getType() {
        return Type;
    }

    /**
     *
     * @param Type
     * The Type
     */
    public void setType(Integer Type) {
        this.Type = Type;
    }

    /**
     *
     * @return
     * The AllowsMultipleValues
     */
    public Boolean getAllowsMultipleValues() {
        return AllowsMultipleValues;
    }

    /**
     *
     * @param AllowsMultipleValues
     * The AllowsMultipleValues
     */
    public void setAllowsMultipleValues(Boolean AllowsMultipleValues) {
        this.AllowsMultipleValues = AllowsMultipleValues;
    }

    /**
     *
     * @return
     * The Options
     */
    public List<Option> getOptions() {
        return Options;
    }

    /**
     *
     * @param Options
     * The Options
     */
    public void setOptions(List<Option> Options) {
        this.Options = Options;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}