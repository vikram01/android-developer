package com.apps.vikram.newtrademe.Model;

import java.util.HashMap;
import java.util.Map;

public class Value {

private String Thumbnail;
private String List;
private String Medium;
private String Gallery;
private String Large;
private String FullSize;
private String PlusSize;
private Integer PhotoId;
private com.apps.vikram.newtrademe.Model.OriginalSize OriginalSize;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The Thumbnail
*/
public String getThumbnail() {
return Thumbnail;
}

/**
* 
* @param Thumbnail
* The Thumbnail
*/
public void setThumbnail(String Thumbnail) {
this.Thumbnail = Thumbnail;
}

/**
* 
* @return
* The List
*/
public String getList() {
return List;
}

/**
* 
* @param List
* The List
*/
public void setList(String List) {
this.List = List;
}

/**
* 
* @return
* The Medium
*/
public String getMedium() {
return Medium;
}

/**
* 
* @param Medium
* The Medium
*/
public void setMedium(String Medium) {
this.Medium = Medium;
}

/**
* 
* @return
* The Gallery
*/
public String getGallery() {
return Gallery;
}

/**
* 
* @param Gallery
* The Gallery
*/
public void setGallery(String Gallery) {
this.Gallery = Gallery;
}

/**
* 
* @return
* The Large
*/
public String getLarge() {
return Large;
}

/**
* 
* @param Large
* The Large
*/
public void setLarge(String Large) {
this.Large = Large;
}

/**
* 
* @return
* The FullSize
*/
public String getFullSize() {
return FullSize;
}

/**
* 
* @param FullSize
* The FullSize
*/
public void setFullSize(String FullSize) {
this.FullSize = FullSize;
}

/**
* 
* @return
* The PlusSize
*/
public String getPlusSize() {
return PlusSize;
}

/**
* 
* @param PlusSize
* The PlusSize
*/
public void setPlusSize(String PlusSize) {
this.PlusSize = PlusSize;
}

/**
* 
* @return
* The PhotoId
*/
public Integer getPhotoId() {
return PhotoId;
}

/**
* 
* @param PhotoId
* The PhotoId
*/
public void setPhotoId(Integer PhotoId) {
this.PhotoId = PhotoId;
}

/**
* 
* @return
* The OriginalSize
*/
public com.apps.vikram.newtrademe.Model.OriginalSize getOriginalSize() {
return OriginalSize;
}

/**
* 
* @param OriginalSize
* The OriginalSize
*/
public void setOriginalSize(com.apps.vikram.newtrademe.Model.OriginalSize OriginalSize) {
this.OriginalSize = OriginalSize;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}