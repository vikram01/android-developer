package com.apps.vikram.newtrademe.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;


public class Attribute {

private String Name;
private String DisplayName;
private String Value;
private Integer Type;
private com.apps.vikram.newtrademe.Model.Range Range;
private List<Option> Options = new ArrayList<Option>();
private Boolean IsRequiredForSell;
private String EncodedValue;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The Name
*/
public String getName() {
return Name;
}

/**
* 
* @param Name
* The Name
*/
public void setName(String Name) {
this.Name = Name;
}

/**
* 
* @return
* The DisplayName
*/
public String getDisplayName() {
return DisplayName;
}

/**
* 
* @param DisplayName
* The DisplayName
*/
public void setDisplayName(String DisplayName) {
this.DisplayName = DisplayName;
}

/**
* 
* @return
* The Value
*/
public String getValue() {
return Value;
}

/**
* 
* @param Value
* The Value
*/
public void setValue(String Value) {
this.Value = Value;
}

/**
* 
* @return
* The Type
*/
public Integer getType() {
return Type;
}

/**
* 
* @param Type
* The Type
*/
public void setType(Integer Type) {
this.Type = Type;
}

/**
* 
* @return
* The Range
*/
public com.apps.vikram.newtrademe.Model.Range getRange() {
return Range;
}

/**
* 
* @param Range
* The Range
*/
public void setRange(com.apps.vikram.newtrademe.Model.Range Range) {
this.Range = Range;
}

/**
* 
* @return
* The Options
*/
public List<Option> getOptions() {
return Options;
}

/**
* 
* @param Options
* The Options
*/
public void setOptions(List<Option> Options) {
this.Options = Options;
}

/**
* 
* @return
* The IsRequiredForSell
*/
public Boolean getIsRequiredForSell() {
return IsRequiredForSell;
}

/**
* 
* @param IsRequiredForSell
* The IsRequiredForSell
*/
public void setIsRequiredForSell(Boolean IsRequiredForSell) {
this.IsRequiredForSell = IsRequiredForSell;
}

/**
* 
* @return
* The EncodedValue
*/
public String getEncodedValue() {
return EncodedValue;
}

/**
* 
* @param EncodedValue
* The EncodedValue
*/
public void setEncodedValue(String EncodedValue) {
this.EncodedValue = EncodedValue;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}