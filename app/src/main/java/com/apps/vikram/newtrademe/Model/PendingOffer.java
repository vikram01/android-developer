package com.apps.vikram.newtrademe.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class PendingOffer {

private Double OfferPrice;
private String OfferExpiryDate;
private Integer Quantity;
private List<Recipient> Recipients = new ArrayList<Recipient>();
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* 
* @return
* The OfferPrice
*/
public Double getOfferPrice() {
return OfferPrice;
}

/**
* 
* @param OfferPrice
* The OfferPrice
*/
public void setOfferPrice(Double OfferPrice) {
this.OfferPrice = OfferPrice;
}

/**
* 
* @return
* The OfferExpiryDate
*/
public String getOfferExpiryDate() {
return OfferExpiryDate;
}

/**
* 
* @param OfferExpiryDate
* The OfferExpiryDate
*/
public void setOfferExpiryDate(String OfferExpiryDate) {
this.OfferExpiryDate = OfferExpiryDate;
}

/**
* 
* @return
* The Quantity
*/
public Integer getQuantity() {
return Quantity;
}

/**
* 
* @param Quantity
* The Quantity
*/
public void setQuantity(Integer Quantity) {
this.Quantity = Quantity;
}

/**
* 
* @return
* The Recipients
*/
public List<Recipient> getRecipients() {
return Recipients;
}

/**
* 
* @param Recipients
* The Recipients
*/
public void setRecipients(List<Recipient> Recipients) {
this.Recipients = Recipients;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}